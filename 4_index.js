(function () {

    const api_service = $.getApiService(); //API服务地址
    let pageW = $(window).width();  //页面宽度
    let pageH = $(window).height(); //页面高度
    var isShark = false;
    //定义变量
    var allParamId = [];    //所有参数ID
    var searchDc = {
        paramIds: "",
        timeType: "day",
        sTime: "",
        eTime: "",
        isQuery: false,
        year: $.getDate("yyyy"),
        month: $.getDate("mm")
    };      //查询条件(数据中心)
    //定时器_6
    let interval_6;
    window.timer_6 = function (playInterval) {
        interval_6 = setInterval(function () { $(".fa-arrow-circle-right").click() }, playInterval)
    };
    window.clear_timer_6 = function () {
        clearInterval(interval_6);
    };

    
    //初始化所有系统
    window.initAllSystem = function () {
        $.reqAjax({
            url: "/api/Cdc_System/All",
            type: "get",
            headers: { "token": true },
            success: function (res) {
                var html = "";
                var carData = []; //焦点图数据
                var data = res.Data;
                if (data.length > 0) {

                    //系统存在，则显示相应的功能模块
                    $(".navbar-nav .nav-item.add-metro").removeClass("hide");   //显示磁贴管理
                    //$(".navbar-nav .nav-item.data-center").removeClass("hide"); //显示数据中心

                    //获取主系统
                    var mainSys = data.filter((item, index) => {

                        var sysId = item.Id;     //系统ID
                        var sysName = item.Name; //系统名称
                        var sysMainPic = "";     //系统主图

                        //获取系统项列表
                        var sysItems = item.Cdc_SystemItems;
                        var mainSysItem = sysItems.filter((item, index) => {

                            var sysItemId = item.Id; //系统项ID
                            var sysItemPic = api_service + item.Picture; //系统项图片

                            if (item.IsMaster) { //获取子系统主图
                                carData.unshift({ "imgUrl": sysItemPic, "sysItemId": sysItemId }); //将主图放在数组的第一位
                                return item;
                            }

                            carData.push({ "imgUrl": sysItemPic, "sysItemId": sysItemId });
                        });
                        if (mainSysItem.length > 0) {
                            sysMainPic = api_service + mainSysItem[0].Picture;
                        }

                        //拼接磁贴系统
                        html += "<div class=\"item\">";
                        html += "<div car-data=" + JSON.stringify(carData) + " metro-type=\"system\" sys-id = \"" + sysId + "\"  class=\"metro-system\">";
                        html += "<div class=\"metro-system-img\">";
                        html += "<img src=\"" + sysMainPic + "\"/>";
                        html += "</div>";
                        html += "<a href=\"javascript:void(0);\" class=\"metro-system-title\">" + sysName + "</a>";
                        html += "</div>";
                        html += "</div>";


                        //获取主系统
                        if (item.IsMaster) {
                            return item;
                        }
                    });

                    //获取主系统项信息
                    var mainSysItem = [];
                    if (mainSys.length > 0) {
                        var playInterval = mainSys[0].PlayInterval; //时间间隔
                        if (playInterval !== 0) {
                            timer_6(playInterval);
                        } else {
                            clear_timer_6();
                        }
                        mainSysItem = mainSys[0].Cdc_SystemItems; //主系统存在，则获取主系统的项
                    } else {
                        mainSysItem = data[0].Cdc_SystemItems; //主系统不存在，则获取第一条系统数据的项
                    }
                    if (mainSysItem.length > 0) {
                       
                        carData = []; //清空默认焦点图
                        mainSysItem.filter((item, index) => {

                            var sysItemId = item.Id; //系统项ID
                            var sysItemPic = api_service + item.Picture; //系统项图片

                            //获取焦点图
                            if (item.IsMaster) {
                                carData.unshift({ "imgUrl": sysItemPic, "sysItemId": sysItemId }); //将主图放在数组的第一位
                                return;
                            }
                            carData.push({ "imgUrl": sysItemPic, "sysItemId": sysItemId });

                        });
                    }
                } else {

                    //系统不存在，则隐藏相应的功能模块
                    $(".navbar-nav .nav-item.add-metro").addClass("hide");   //隐藏磁贴管理
                    //$(".navbar-nav .nav-item.data-center").addClass("hide"); //隐藏数据中心

                    $("#parameter-sidebar").css("width", "0px"); //隐藏系统参数层
                    $("#metro-main").css("width", "0px");        //隐藏功能磁贴层
                }
                /*carData.PlayInterval = playInterval;*/

                //添加磁贴系统
                $("#metroSystem").html(html);

                getCarousel(carData, true);
            }
        })
    }
    
    initAllSystem();

    //获取系统焦点图的磁贴
    window.getCarousel = function (data, isInit) {
        $(".carousel-inner").html(""); //清空焦点图
        if (data.length > 0) {
            let videoObj = null;

           /* var playInterval1 = data.PlayInterval;*/
            for (i = 0; i < data.length; i++) {
                var imgUrl = data[i].imgUrl;       //图片地址
                var sysItemId = data[i].sysItemId; //系统项ID
                var html = "";
                $.reqAjax({
                    url: "/api/Cdc_SystemItemMetro/GetMeteros/" + sysItemId + "",
                    type: "get",
                    async: false, //同步读取
                    headers: { "token": true },
                    success: function (res) {
                        var arrChart = [];
                        var data = res.Data;

                        if (data.length > 0) {
                            for (var i = 0; i < data.length; i++) {

                                var id = data[i].Id;          //磁铁ID
                                var metroName = data[i].Name; //磁铁名称
                                var x = data[i].X; //X轴
                                var y = data[i].Y; //Y轴
                                let pageW = 0;     //页面宽度
                                let pageH = 0;     //页面高度
                                let appKey = "";     //appKey
                                let appSecret = "";     //appSecret
                                let deviceSerial = "";     //deviceSerial
                                let deviceCode = "";     //deviceCode
                                let jsonConfig = eval("(" + data[i].JsonConfig + ")"); //Json配置数据
                                if (!$.isEmpty(jsonConfig)) {
                                    pageW = parseFloat(jsonConfig.pageW);
                                    pageH = parseFloat(jsonConfig.pageH);
                                    appKey = jsonConfig.appKey;          //AppKey
                                    appSecret = jsonConfig.appSecret;          //AppSecret
                                    deviceSerial = jsonConfig.deviceSerial;          //DeviceSerial
                                    deviceCode = jsonConfig.deviceCode;          //DeviceCode
                                }

                                //设置参数
                                var textFontSize = data[i].TextFontSize; //字体大小
                                var textColor = data[i].TextColor;       //字体颜色

                                //设置参数值
                                var fontSize = data[i].FontSize;       //字体大小
                                var dataColor = data[i].DataColor;     //字体颜色
                                var bgColor = data[i].BackgroundColor; //背景颜色
                                

                                //磁贴参数
                                var metroParams = data[i].Cdc_SystemItemMetroParameters;

                                var viewData = null; //查看数据
                                var pId = "";        //参数ID
                                var pName = "";      //参数名称
                                var pValue = "";     //参数值
                                var unit = "";       //参数单位
                                var dataType = 0;    //数据类型
                                //磁贴类型
                                var metroType = data[i].MetroType;
                                switch (metroType) {
                                    case 0: //磁贴参数(添加实时)
                                        {
                                            pId = data[i].Sys_ParameterId;       //参数ID
                                            pName = data[i].Name;                //参数名称
                                            var isShowName = data[i].IsShowName; //是否显示参数名称
                                            unit = data[i].Sys_Parameter.Unit;   //参数单位
                                            viewData = data[i].ViewData;
                                            if (!$.isEmpty(viewData)) {
                                                pValue = viewData.Val;        //参数值
                                                dataType = viewData.DataType; //数据类型
                                            }

                                            //是否显示背景颜色与边框
                                            let border = ""; //边框
                                            if (bgColor.indexOf("@") >= 0) {
                                                let colArr = bgColor.split("@");
                                                bgColor = colArr[0];
                                                let isShowBgColor = colArr[1]; 
                                                if (isShowBgColor === "false") {
                                                    bgColor = "none";
                                                    border = "border: none;";
                                                }
                                            }

                                            //添加实时参数ID
                                            res = allParamId.some(item => {
                                                if (item.Id === pId && item.DataType === dataType) {
                                                    return true
                                                }
                                            })
                                            if (!res) {
                                                allParamId.push({ Id: pId, DataType: dataType });
                                            }

                                            html +=
                                                '<a metro-type="param" real-param-id=' + pId + dataType + ' elem-index="param-' + id + '" elem-id="' + id + '" elem-left=' + x + ' elem-top=' + y + ' elem-pageW=' + pageW + ' elem-pageH=' + pageH + ' style="font-size:' + textFontSize + 'px;color:' + textColor + ';position: absolute; left: ' + x + 'px; top: ' + y + 'px;z-index: 9;" class="jstree-anchor jstree-hovered ui-draggable ui-draggable-handle ui-draggable-dragging" href="#" tabindex="-1" role="treeitem" aria-selected="false" aria-level="2" id="' + pId + '_anchor">' + (isShowName === true ? pName : "") + '' +
                                                '<div class="jstree-data"><span style="font-size:' + fontSize + 'px;color:' + dataColor + ';background:' + bgColor + ';' + border + '"><em class="value">' + (pValue === null || pValue === "" ? "&nbsp" : pValue) + '</em> ' + (unit === null || unit === "" ? "&nbsp" : unit) + '</span></div>' +
                                                '</a>';

                                            break;
                                        }
                                    case 1:  //系统
                                        var carData = []; //焦点图数据
                                        var sysMainPic = ""; //系统主图
                                        var system = data[i].Cdc_System;
                                        if (!$.isEmpty(system)) {
                                            var sysName = data[i].Cdc_System.Name; //系统名称
                                            var sysItem = data[i].Cdc_System.Cdc_SystemItems; //系统项列表
                                            if (sysItem.length > 0) {
                                                sysItem.filter((item, index) => {
                                                    if (item.IsMaster) {
                                                        carData.unshift({ "imgUrl": api_service + item.Picture, "sysItemId": item.Id }); //将主图放在数组的第一位
                                                        sysMainPic = api_service + item.Picture;
                                                        return;
                                                    }
                                                    carData.push({ "imgUrl": api_service + item.Picture, "sysItemId": item.Id });
                                                });
                                            }

                                            html +=
                                                '<div car-data=' + JSON.stringify(carData) + ' metro-type="system" class="metro-system" elem-index="system-' + id + '" elem-id=' + id + ' elem-left=' + x + ' elem-top=' + y + ' elem-pageW=' + pageW + ' elem-pageH=' + pageH + ' style="position: absolute; z-index: 9; left: ' + x + 'px; top: ' + y + 'px;">' +
                                                '<div class="metro-system-img">' +
                                                '<img src=' + sysMainPic + ' />' +
                                                '</div>' +
                                                '<a href="javascript:void(0);" class="metro-system-title">' + sysName + '</a>' +
                                                '</div>';
                                        }
                                        break;
                                    case 2: //参数值磁铁数据(添加实时)
                                        if (metroParams.length > 0) {
                                            html += "<div metro-type=\"metro\" class=\"item total-active-energy ui-draggable ui-draggable-handle ui-draggable-dragging\" elem-index=\"metro-" + id + "\" elem-id=\"" + id + "\" elem-left=\"" + x + "\" elem-top=\"" + y + "\" elem-pageW=\"" + pageW + "\" elem-pageH=\"" + pageH + "\" style=\"position: absolute; z-index: 9; left:" + x + "px; top:" + y + "px;font-size:" + fontSize + "px;color:" + textColor + ";background:" + bgColor + " \">";
                                        html += "<div class=\"metro-card-title\">" + metroName + "</div>";
                                            html += "<div class=\"metro-card-data\">";
                                            html += "<div class=\"metro-energy-list\">";
                                            html += "<ul>";
                                            /*html += ' <table style="text-align: center;border: 1px solid rgb(255, 255, 255);"><thead > <tr> <th colspan="2"  style="text-align: center;border: 1px solid rgb(255, 255, 255);">' + metroName + '</th> </tr> </thead><tbody>';*/
                                            html += ' <table style="text-align: center;border: 1px solid rgb(255, 255, 255);"><tbody>';

                                            if (metroParams.length > 0) {
                                                for (var j = 0; j < metroParams.length; j++) {
                                                    pId = metroParams[j].Sys_Parameter.Id;          //参数ID
                                                    pName = metroParams[j].Sys_Parameter.Name;      //参数名称
                                                    unit = metroParams[j].Sys_Parameter.Unit ?? "&nbsp"; //参数单位
                                                    viewData = metroParams[j].ViewData; //视图数据
                                                    if (!$.isEmpty(viewData)) {
                                                        pValue = viewData.Val ?? "&nbsp";  //参数值
                                                        dataType = viewData.DataType;      //数据类型
                                                    }


                                                    //添加实时参数ID
                                                    res = allParamId.some(item => {
                                                        if (item.Id === pId && item.DataType === dataType) {
                                                            return true
                                                        }
                                                    })
                                                    if (!res) {
                                                        allParamId.push({ Id: pId, DataType: dataType });
                                                    }
                                                    let tr = '<tr> <td  style="text-align: center;border: 1px solid rgb(255, 255, 255);">' + pName + '</td> <td  style="text-align: center;border: 1px solid rgb(255, 255, 255);">' + pValue + ' ' + unit + '</td> </tr>';
                                                    html += tr;
                                                    //html += "<li real-param-id=" + pId + dataType + " style='border-bottom: 1px solid #ccc'><b>" + pName + "：</b><span class=\"value\">" + pValue + "</span> " + unit + "</li>";
                                                }
                                                html += '</tbody></table>';
                                            }

                                            html += "</ul>";
                                            html += "</div>";
                                            html += "</div>";
                                            html += "</div>";
                                        } else {
                                            //初始化自定义文字
                                            html += "<div metro-type=\"customText\" class=\"custom-text ui-draggable ui-draggable-handle ui-draggable-dragging\" elem-index=\"customText-" + id + "\" elem-id=\"" + id + "\" elem-left=\"" + x + "\" elem-top=\"" + y + "\" elem-pageW=\"" + pageW + "\" elem-pageH=\"" + pageH + "\" style=\"position: absolute; z-index: 9; left:" + x + "px; top:" + y + "px;font-size:" + fontSize + "px;color:" + textColor + ";background:" + bgColor + " \">";
                                            html += "<div class=\"metro-card-title\">" + metroName + "</div>";
                                            html += "</div>";
                                        }
                                        break;
                                    case 9: //视频配置数据(添加实时)
                                        debugger
                                        videoObj = initVideo(data[i]);
                                        console.log(videoObj)
                                        html += videoObj.object;
                                        break;
                                    case 3:  //设备
                                        {
                                            //设备数量
                                            let deviceNumber = jsonConfig.deviceNumber;
                                            if ($.isEmpty(deviceNumber) || deviceNumber === "1") {
                                                deviceNumber = "";
                                            }

                                            html +=
                                                '<div metro-type="device" class="item equipment-energy w2 ui-draggable ui-draggable-handle ui-draggable-dragging" elem-index=' + 'device-' + id + ' elem-id=' + id + ' elem-left=' + x + ' elem-top=' + y + ' elem-pageW=' + pageW + ' elem-pageH=' + pageH + ' style="position: absolute; z-index: 9; left: ' + x + 'px; top: ' + y + 'px;">' +
                                                '<div title="Equipment Operation Status" class="equipment-energy-icon">' +
                                                '<img src="images/equipment-icon.png" alt="">' +
                                                '<span class="number">' + deviceNumber +'</span>' +
                                                '</div>' +
                                                '<div class="metro-card-title">Equipment</div>' +
                                                '<div class="metro-card-data">' +
                                                '<div class="metro-equipment-energy">' +
                                                '<img src="images/equipment-energy.png">' +
                                                '<ul>' +
                                                '<li>Brand: Trane</li>' +
                                                '<li>Power: 500kW</li>' +
                                                '<li>Cooling Cap: 900 RT</li>' +
                                                '<li>Installation: 2020.8.1</li>' +
                                                '</ul>' +
                                                '</div>' +
                                                '</div>' +
                                                '</div>';
                                            break;
                                        }
                                    case 4:  //开关(添加实时)
                                        
                                        var displayContents = data[i].DisplayContents; //显示内容
                                        pId = data[i].Sys_ParameterId;    //参数ID
                                        viewData = data[i].ViewData;      //显示数据
                                        if (!$.isEmpty(viewData)) {
                                            dataType = viewData.DataType; //数据类型
                                            let val = viewData.Val; //值

                                            //显示当前开关
                                            let result = displayContents.filter((item, index) => {
                                                if (item.Val === val) {
                                                    return item;
                                                }
                                            });
                                            if (result.length > 0) {
                                                let picture = api_service + result[0].Picture; //显示开关图片
                                                html +=
                                                    '<div real-param-id=' + pId + dataType + ' param-content=' + JSON.stringify(displayContents) + ' metro-type="switch" class="item operation-status ui-draggable ui-draggable-handle ui-draggable-dragging"  elem-index=' + "switch-" + id + ' elem-id=' + id + ' elem-left=' + x + ' elem-top=' + y + ' elem-pageW=' + pageW + ' elem-pageH=' + pageH + ' style="position: absolute; z-index: 9; left: ' + x + 'px; top: ' + y + 'px;">' +
                                                    '<div class="metro-card-data">' +
                                                    '<div class="metro-operation-status">' +
                                                    '<div class="metro-operation-status-col"><img src=' + picture + '></div>' +
                                                    '</div>' +
                                                    '</div>' +
                                                    '</div>';
                                            } else {
                                                html +=
                                                    '<div real-param-id=' + pId + dataType + ' param-content=' + JSON.stringify(displayContents) + ' metro-type="switch" class="item operation-status ui-draggable ui-draggable-handle ui-draggable-dragging"  elem-index=' + "switch-" + id + ' elem-id=' + id + ' elem-left=' + x + ' elem-top=' + y + ' elem-pageW=' + pageW + ' elem-pageH=' + pageH + ' style="position: absolute; z-index: 9; left: ' + x + 'px; top: ' + y + 'px;">' +
                                                    '<div class="metro-card-data">' +
                                                    '<div class="metro-operation-status">' +
                                                    '<div class="metro-operation-status-col"><img style="width:35px!important;height: 35px!important;" src="/images/error-status.png"><span style="display:block;padding-top:2px;color:#ee2630;">配置错误</span></div>' +
                                                    '</div>' +
                                                    '</div>' +
                                                    '</div>';
                                            }
                                        }

                                        break;
                                    case 7: //磁贴图表
                                        {
                                            var chartConfig = data[i].ChartConfig; //图表配置数据
                                            var arrParamId = [];
                                            if (metroParams.length > 0) {
                                                for (var k = 0; k < metroParams.length; k++) {
                                                    arrParamId.push(metroParams[k].Sys_Parameter.Id); //参数ID
                                                }
                                            }
                                            var paramIds = arrParamId.join(","); //将数组转换为逗号字符串

                                            //图表磁贴尺寸
                                            let metroW = 304; //磁贴宽度
                                            let metroH = 299; //磁贴高度
                                            let chartW = 270; //图表宽度
                                            let chartH = 240; //图表高度
                                            let chartMetro = jsonConfig.chartMetro;
                                            if (!$.isEmpty(chartMetro)) {
                                                metroW = chartMetro.width;
                                                metroH = chartMetro.height;
                                                chartW = metroW - 30;
                                                chartH = metroH - 59;
                                            }

                                            html +=
                                                '<div metro-type="chart" class="item parameter-energy w2 h2 ui-draggable ui-draggable-handle ui-draggable-dragging" elem-index="chart-' + id + '" elem-left=' + x + ' elem-top=' + y + ' elem-pageW=' + pageW + ' elem-pageH=' + pageH + ' style="position: absolute; z-index: 9; left: ' + x + 'px; top: ' + y + 'px; width: ' + metroW + 'px; height: ' + metroH + 'px; font-size: ' + fontSize + 'px; color: ' + textColor + '; background: ' + bgColor + ';" elem-id=' + id + '>' +
                                                '<div class="metro-card-title">' +
                                                '<span>' + metroName + '</span>' +
                                                '<div class="time-group float-right">' +
                                                '<a param-ids=' + paramIds + ' chart-id="chart-' + id + '" time-type="hour" class="left">时</a>' +
                                                '<a param-ids=' + paramIds + ' chart-id="chart-' + id + '" time-type="day" class="right on">日</a>' +
                                                '</div>' +
                                            '</div>' +
                                            '<div class="metro-card-data"><div id="chart-' + id + '" style="width: ' + chartW + 'px; height: ' + chartH + 'px;"></div></div>' +
                                                '</div>';

                                            arrChart.push({ elemId: "chart-" + id + "", data: chartConfig, bgColor: bgColor });

                                            break;
                                        }
                                    case 8: //磁贴图表
                                        {
                                            
                                       
                                            var ifrme = drawnew();
                                            html += '<div metro-type="new"  class="item information-news ui-draggable ui-draggable-handle ui-draggable-dragging"  elem-left=' + x + ' elem-top=' + y + ' elem-index="new-' + id + '" elem-id="' + id + '" style="background-color:transparent; position: absolute; z-index: 9; left: '+x+'px; top: '+y+'px;">';
                                             
                                            html += ifrme;
                                            html +='</div> '
                                                   
                                            

                                            break;
                                        }
                                }
                            }
                        }

                        $(".carousel-inner").append("<div sys-item-id=\"" + sysItemId + "\" class=\"carousel-item\"><div class=\"divRight\">" + html + "</div><img id=\"img-" + id + "\" src=\"" + imgUrl + "\" class=\"d-block bg-img\"></div>");
                        $(".carousel-item").first().addClass("active");
                        $(".carousel-indicators > li").first().addClass("active");
                        $("#carouselExampleControls").carousel();
                       
                        //循环渲染图表（这里需要等待焦点图元素加载完成后加载图表）
                        if (arrChart.length > 0) {
                            for (var m = 0; m < arrChart.length; m++) {
                                renderMetroChart(arrChart[m].elemId, arrChart[m].data, arrChart[m].bgColor);
                            }
                        }

                        autoResizePosition(pageW, pageH); //自动调整元素位置
                        if (metroType == '9' && null !== videoObj) {
                            var elem = $(".carousel-inner .video-text[elem-index=" +"videoText-"+ videoObj.id + "]");
                            //       $(elem)
                            //         .attr({ "elem-id": data.Id, "elem-left": data.x, "elem-top": data.y, "elem-pageW": jsonConfig.pageW, "elem-pageH": jsonConfig.pageH })
                            let width = videoObj.width;
                            let height = videoObj.height;
                            let videoKey = videoObj.videoKey;
                            $(elem).children(".metro-card-title").html('摄像头');
                            $(elem).children(".metro-card-data").html("<div id=" + videoKey + "></div>");
                            setTimeout(1000)
                            window.parent.ezopenVideo(videoKey, 270, 250); //iframe调用外部index页面方法
                            window.parent.stopvideo(videoKey);
                        }
                    }
                })
            }
           

            //动态绑定实时参数ID
            bindParamIds(allParamId);

            //初始化动态元素拖拽（开关）
            initDynElemDrag("switch");

            //初始化动态元素拖拽（设备）
            initDynElemDrag("device");

            //初始化动态元素拖拽（磁贴网格系统）
            if (isInit) {
                initDynElemDrag("metro-system");
            }

            //初始化动态元素拖拽（自定义文字）
            initDynElemDrag("customText");

            //初始化动态元素拖拽（焦点图系统）
            initDynElemDrag("carousel-system");

            //初始化动态元素拖拽（磁贴数据）
            initDynElemDrag("metro");

            //初始化动态元素拖拽（视频配置）
            initDynElemDrag("videoText");

            //初始化动态元素拖拽（磁贴参数）
            initDynElemDrag("param");

            //初始化动态元素拖拽（磁贴图表）
            initDynElemDrag("chart");
            initDynElemDrag("new");
        }
        else {

            //默认焦点图数据
            var dfData = [
                { "imgUrl": "images/system-bg-01.jpg", "sysItemId": "" },
                //{ "imgUrl": "images/system-bg-02.jpg", "sysItemId": "" },
                //{ "imgUrl": "images/system-bg-03.jpg", "sysItemId": "" }
            ];

            for (var i = 0; i < dfData.length; i++) {
                $(".carousel-inner").append("<div sys-item-id=\"" + dfData[i].sysItemId + "\" class=\"carousel-item\"><div class=\"divRight\"></div><img src=\"" + dfData[i].imgUrl + "\" class=\"d-block bg-img\"></div>");
                $(".carousel-item").first().addClass("active");
                $(".carousel-indicators > li").first().addClass("active");
                $("#carouselExampleControls").carousel();
               
               
            }
        }
    };

    //浏览器缩放事件
    $(window).resize(function () {
        let pageW = $(window).width();  //页面缩放后的宽度
        let pageH = $(window).height(); //页面缩放后的高度
        autoResizePosition(pageW, pageH); //自动调整元素位置
    });

    //自动调整元素位置
    window.autoResizePosition = function (currW, currH) {

        $(".carousel-inner .operation-status,.carousel-inner .metro-system,.carousel-inner .equipment-energy,.carousel-inner .jstree-anchor,.carousel-inner .total-active-energy,.carousel-inner .parameter-energy,.information-news,.custom-text,.video-text").each(function () {

            //根据磁贴类型获取元素高度
            let elemH = 0;
            let metroType = $(this).attr("metro-type");
            switch (metroType) {
                case "switch": //开关
                    elemH = $(this).find("img").height() + 8;
                    break;
                case "device": //设备
                    elemH = $(this).height();
                    break;
                case "param": //参数
                    elemH = $(this).height();
                    break;
                case "system": //系统
                    elemH = $(this).height();
                    break;
                case "metro": //参数值磁贴数据
                    elemH = $(this).height() + 18;
                    break;
                case "videoText": //视频配置
                    elemH = $(this).height() + 18;
                    break;
                case "chart": //磁贴图表
                    elemH = $(this).height() + 18;
                    break;
            }

            //调整左边缘位置
            let origL = parseFloat($(this).attr("elem-left")); //元素原有左边缘
            let pageW = parseFloat($(this).attr("elem-pageW")); //页面宽度
            if (pageW !== 0 && pageW !== currW) {
                let left = currW / pageW * origL; //等比计算左边缘
                $(this).css("left", left + "px"); //设置元素左边缘
            } else {
                $(this).css("left", origL + "px"); //设置元素左边缘
            }

            //调整上边缘位置
            let origT = parseFloat($(this).attr("elem-top"));   //元素原有上边缘
            let pageH = parseFloat($(this).attr("elem-pageH")); //最大高度
            if (pageH !== 0 && pageH !== currH) {
                let top = currH / pageH * origT; //等比计算上边缘
                let total = top + elemH; //合计上边缘
                if (elemH > 0 && total > currH) {
                    top = top - (total - currH); //检查元素是否超出范围
                }
                $(this).css("top", top + "px"); //设置元素上边缘
            } else {
                $(this).css("top", origT + "px"); //设置元素上边缘
            }
        });
    };

    //初始化元素拖拽（磁贴网格开关|设备）
    var initElemDrag = function () {
        $(".metro-grid .operation-status,.equipment-energy,.total-active-energy,.parameter-energy,.information-news,.custom-text,.video-text").draggable({
            helper: "clone" //复制元素
        }).on('dragstart', function (e, ui) { //拖拽元素绑定事件
            $(ui.helper).css('z-index', '9');
        }).on('dragstop', function (e, ui) {  //拖拽元素停止事件
            var elemIndex = "";            //元素索引
            var x1 = ui.position.left;     //X轴
            var y1 = ui.position.top;      //Y轴
            let pageW = $(window).width();  //页面宽度
            let pageH = $(window).height(); //页面高度

            var metroType = $(this).attr("selectname");

            //检查磁贴类型
            var metroType = $(this).attr("metro-type");
            if (!$.isEmpty(metroType)) {
                switch (metroType) {
                    case "switch": //开关
                        elemIndex = "switch-" + $.getDate("yyyyMMddHHmmssfff");

                        //弹出页面
                        $.layer.open({
                            title: "新增开关", content: "switch/add.html?elemIndex=" + elemIndex + "&x=" + x1 + "&y=" + y1 + "&pageW=" + pageW + "&pageH=" + pageH + "", width: "570px", height: "700px", shadeClose: false,
                            cancel: function () {
                                $(".carousel-inner .operation-status[elem-index=" + elemIndex + "]").remove(); //移除开关磁贴
                            }
                        });

                        //添加元素索引
                        $(".carousel-item.active .divRight").append($(ui.helper).attr("elem-index", elemIndex).clone().draggable());


                        initDynElemDrag("switch");

                        break;
                    case "device": //设备
                        elemIndex = "device-" + $.getDate("yyyyMMddHHmmssfff");

                        //弹出页面
                        $.layer.open({
                            title: "新增设备", content: "device/manage.html?oper=add&elemIndex=" + elemIndex + "&x=" + x1 + "&y=" + y1 + "&pageW=" + pageW + "&pageH=" + pageH + "", width: "1000px", height: "750px", shadeClose: false,
                            cancel: function () {
                                $(".carousel-inner .equipment-energy[elem-index=" + elemIndex + "]").remove(); //移除设备磁贴
                            }
                        });

                        //添加元素索引
                        $(".carousel-item.active .divRight").append($(ui.helper).attr("elem-index", elemIndex).clone().draggable());

                        initDynElemDrag("device");

                        break;
                    case "metro": //参数值磁铁数据
                        elemIndex = "metro-" + $.getDate("yyyyMMddHHmmssfff");

                        //弹出页面
                        $.layer.open({
                            title: "新增参数值", content: "metro/data.html?oper=add&elemIndex=" + elemIndex + "&x=" + x1 + "&y=" + y1 + "&pageW=" + pageW + "&pageH=" + pageH + "", width: "650px", height: "750px", shadeClose: false,
                            cancel: function () {
                                $(".carousel-inner .total-active-energy[elem-index=" + elemIndex + "]").remove(); //移除磁贴数据
                            }
                        });

                        //添加元素索引
                        $(".carousel-item.active .divRight").append($(ui.helper).attr("elem-index", elemIndex).clone().draggable());

                        initDynElemDrag("metro");

                        break;
                    case "videoText": //视频磁铁数据
                        elemIndex = "videoText-" + $.getDate("yyyyMMddHHmmssfff");

                        //弹出页面
                        $.layer.open({
                            title: "摄像头配置参数", content: "metro/videoConfig.html?oper=add&elemIndex=" + elemIndex + "&x=" + x1 + "&y=" + y1 + "&pageW=" + pageW + "&pageH=" + pageH + "", width: "700px", height: "550px", shadeClose: false,
                            cancel: function () {
                                $(".carousel-inner .video-text[elem-index=" + elemIndex + "]").remove(); //移除磁贴数据
                            }
                        });

                        //添加元素索引
                        $(".carousel-item.active .divRight").append($(ui.helper).attr("elem-index", elemIndex).clone().draggable());

                        initDynElemDrag("videoText");

                        break;
                    case "customText": //自定义文字磁贴数据
                        elemIndex = "customText-" + $.getDate("yyyyMMddHHmmssfff");

                        //弹出页面
                        $.layer.open({
                            title: "新增自定义文字", content: "metro/customText.html?oper=add&elemIndex=" + elemIndex + "&x=" + x1 + "&y=" + y1 + "&pageW=" + pageW + "&pageH=" + pageH + "", width: "450px", height: "550px", shadeClose: false,
                            cancel: function () {
                                $(".carousel-inner .custom-text[elem-index=" + elemIndex + "]").remove(); //移除磁贴数据
                            }
                        });

                        //添加元素索引
                        $(".carousel-item.active .divRight").append($(ui.helper).attr("elem-index", elemIndex).clone().draggable());

                        initDynElemDrag("customText");

                        break;
                    
                    case "chart":

                        elemIndex = "chart-" + $.getDate("yyyyMMddHHmmssfff");

                        //弹出页面
                        $.layer.open({
                            title: "新增磁贴数据", content: "metro/chart.html?oper=add&elemIndex=" + elemIndex + "&x=" + x1 + "&y=" + y1 + "&pageW=" + pageW + "&pageH=" + pageH + "", width: "650px", height: "750px", shadeClose: false,
                            cancel: function () {
                                $(".carousel-inner .parameter-energy[elem-index=" + elemIndex + "]").remove(); //移除磁贴数据
                            }
                        });

                        //添加元素索引
                        $(".carousel-item.active .divRight").append($(ui.helper).attr("elem-index", elemIndex).clone().draggable());

                        initDynElemDrag("chart");

                        break;
                    case "new":
                        var id = $.getDate("yyyyMMddHHmmssfff");
                        elemIndex = "new-" + id;

                        //弹出页面

                        //添加元素索引
                        $(".carousel-item.active .divRight").append($(ui.helper).attr("elem-index", elemIndex).clone().addClass("new").draggable().css("background-color", "transparent").html(drawnew()));

                      
                        var dataObj = { metroType: 8, elemIndex: elemIndex, x: x1, y: y1 };
                        addSystemMetro($(this), dataObj);



                        initDynElemDrag("new");

                        break;
                }
            }
        });
    }
    initElemDrag();

    //初始化动态元素拖拽
    window.initDynElemDrag = function (metroType) {
        switch (metroType) {
            case "switch":
                $(".carousel-inner .operation-status").draggable({

                }).on('dragstart', function (e, ui) { //拖拽元素绑定事件
                    $(ui.helper).css("z-index", "9");
                }).on('dragstop', function (e, ui) {  //拖拽元素停止事件
                    var x = ui.position.left; //x轴
                    var y = ui.position.top;  //y轴
                    var elemId = $(this).attr("elem-id"); //元素ID
                    if (!$.isEmpty(elemId)) {
                        updateMetroPoint($(this), elemId, x, y, metroType);//修改磁贴坐标
                    }
                });
                break;
            case "device":
                $(".carousel-inner .equipment-energy").draggable({

                }).on('dragstart', function (e, ui) { //拖拽元素绑定事件
                    $(ui.helper).css("z-index", "9");
                }).on('dragstop', function (e, ui) {  //拖拽元素停止事件
                    var x = ui.position.left; //x轴
                    var y = ui.position.top;  //y轴
                    var elemId = $(this).attr("elem-id"); //元素ID
                    if (!$.isEmpty(elemId)) {
                        updateMetroPoint($(this), elemId, x, y, metroType); //修改磁贴坐标
                    }
                });
                break;
            case "customText":
                $(".carousel-inner .custom-text").draggable({

                }).on('dragstart', function (e, ui) { //拖拽元素绑定事件
                    $(ui.helper).css("z-index", "9");
                }).on('dragstop', function (e, ui) {  //拖拽元素停止事件
                    var x = ui.position.left; //x轴
                    var y = ui.position.top;  //y轴
                    var elemId = $(this).attr("elem-id"); //元素ID
                    if (!$.isEmpty(elemId)) {
                        updateMetroPoint($(this), elemId, x, y, metroType); //修改磁贴坐标
                    }
                });
                break;
            case "metro-system":
                $(".metro-grid .metro-system").draggable({
                    helper: "clone" //复制元素
                }).on('dragstart', function (e, ui) { //拖拽元素绑定事件
                    $(ui.helper).css("z-index", "9");
                }).on('dragstop', function (e, ui) {  //拖拽元素停止事件

                    var x1 = ui.position.left; //x轴
                    var y1 = ui.position.top;  //y轴
                    var sysId = $(this).attr("sys-id"); //系统ID
                    var elemIndex = "system-" + $.getDate("yyyyMMddHHmmssfff");

                    //添加元素索引
                    $(".carousel-item.active .divRight").append($(ui.helper).attr("elem-index", elemIndex).addClass("new").clone().draggable());

                    //新增系统磁贴表
                    var dataObj = { metroType: 1, sysId: sysId, elemIndex: elemIndex, x: x1, y: y1 };
                    addSystemMetro($(this),dataObj);

                    //拖拽之后的元素（必须要添加二次拖拽事件，否则无效）
                    $(".carousel-inner .metro-system.new").draggable({

                    }).on('dragstart', function (e, ui) { //拖拽元素绑定事件
                        $(ui.helper).css("z-index", "9");
                    }).on('dragstop', function (e, ui) {  //拖拽元素停止事件

                        var x = ui.position.left; //x轴
                        var y = ui.position.top;  //y轴
                        var elemId = $(this).attr("elem-id"); //元素ID
                        if (!$.isEmpty(elemId)) {
                            updateMetroPoint($(this), elemId, x, y, metroType); //修改磁贴坐标
                        }
                    });

                });
                break;
            case "carousel-system":

                $(".carousel-inner .metro-system").draggable({

                }).on('dragstart', function (e, ui) { //拖拽元素绑定事件
                    $(ui.helper).css("z-index", "9");
                }).on('dragstop', function (e, ui) {  //拖拽元素停止事件

                    var x = ui.position.left; //x轴
                    var y = ui.position.top;  //y轴
                    var elemId = $(this).attr("elem-id"); //元素ID
                    if (!$.isEmpty(elemId)) {
                        updateMetroPoint($(this), elemId, x, y, metroType); //修改磁贴坐标
                    }
                });
                break;
            case "videoText":
                $(".carousel-inner .video-text").draggable({

                }).on('dragstart', function (e, ui) { //拖拽元素绑定事件
                    $(ui.helper).css("z-index", "9");
                }).on('dragstop', function (e, ui) {  //拖拽元素停止事件
                    var x = ui.position.left; //x轴
                    var y = ui.position.top;  //y轴
                    var elemId = $(this).attr("elem-id"); //元素ID
                    if (!$.isEmpty(elemId)) {
                        updateMetroPoint($(this), elemId, x, y, metroType); //修改磁贴坐标
                    }
                });
                $(".carousel-inner .video-text").resizable({
                    //maxHeight: 250,
                    //maxWidth: 350,
                    minWidth: 250,  //最小宽度
                    minHeight: 250, //最小高度
                    start: function (event, ui) {
                    },
                    stop: function (event, ui) {
                        let resizeW = ui.size.width;  //调整宽度
                        let resizeH = ui.size.height; //调整高度
                        let left = ui.originalPosition.left;//左边缘位置
                        let top = ui.originalPosition.top;  //上边缘位置

                        let the = $(ui.helper);             //拖拽元素
                        let elemId = the.attr("elem-id");   //拖拽元素ID
                        let pageW = the.attr("elem-pageW"); //页面宽度
                        let pageH = the.attr("elem-pageH"); //页面高度
                    },
                    resize: function (event, ui) {
                        let resizeW = ui.size.width;  //调整宽度
                        let resizeH = ui.size.height; //调整高度
                        let chartW = resizeW - 30;    //图表宽度
                        let chartH = resizeH - 59;    //图表高度
                        let elemId = $(ui.helper).attr("elem-id"); //拖拽元素ID

                        if (window.EZOPENDemo) {
                            window.EZOPENDemo.reSize(chartW, chartH)
                        }
                        var table = $('.carousel-inner .video-text table');

                        // 设置表格宽度自适应父元素
                        table.css('table-layout', 'fixed');
                        table.css('width', '100%');
                        // 调整表格高度
                        var tableHeight = $('.carousel-inner .video-text').height() * 0.7; //假设表格高度为父元素高度的80％
                        table.height(tableHeight);
                    }
                });
                break;
            case "metro":
                $(".carousel-inner .total-active-energy").draggable({

                }).on('dragstart', function (e, ui) { //拖拽元素绑定事件
                    $(ui.helper).css("z-index", "9");
                }).on('dragstop', function (e, ui) {  //拖拽元素停止事件
                    var x = ui.position.left; //x轴
                    var y = ui.position.top;  //y轴
                    var elemId = $(this).attr("elem-id"); //元素ID
                    if (!$.isEmpty(elemId)) {
                        updateMetroPoint($(this), elemId, x, y, metroType); //修改磁贴坐标
                    }
                });
                $(".carousel-inner .total-active-energy").resizable({
                    //maxHeight: 250,
                    //maxWidth: 350,
                    minWidth: 150,  //最小宽度
                    minHeight: 150, //最小高度
                    start: function (event, ui) {
                    },
                    stop: function (event, ui) {
                        let resizeW = ui.size.width;  //调整宽度
                        let resizeH = ui.size.height; //调整高度
                        let left = ui.originalPosition.left;//左边缘位置
                        let top = ui.originalPosition.top;  //上边缘位置

                        let the = $(ui.helper);             //拖拽元素
                        let elemId = the.attr("elem-id");   //拖拽元素ID
                        let pageW = the.attr("elem-pageW"); //页面宽度
                        let pageH = the.attr("elem-pageH"); //页面高度
                    },
                    resize: function (event, ui) {
                        let resizeW = ui.size.width;  //调整宽度
                        let resizeH = ui.size.height; //调整高度
                        let chartW = resizeW - 30;    //图表宽度
                        let chartH = resizeH - 59;    //图表高度
                        let elemId = $(ui.helper).attr("elem-id"); //拖拽元素ID

                        var table = $('.carousel-inner .total-active-energy table');

                        // 设置表格宽度自适应父元素
                        table.css('table-layout', 'fixed');
                        table.css('width', '100%');
                        // 调整表格高度
                        var tableHeight = $('.carousel-inner .total-active-energy').height() * 0.7; //假设表格高度为父元素高度的80％
                        table.height(tableHeight);

                       
                    }
                });
                break;
            case "tree-param":
                $("#parameter-sidebar .metro-tree a.jstree-anchor[aria-level!='1']").draggable({
                    helper: "clone" //复制元素
                }).on('dragstart', function (e, ui) { //拖拽元素绑定事件
                    $(ui.helper).css("z-index", "9");
                }).on('dragstop', function (e, ui) {  //拖拽元素停止事件
                    var x = ui.position.left; //x轴
                    var y = ui.position.top;  //y轴
                    var elemIndex = "param-" + $.getDate("yyyyMMddHHmmssfff");

                    //添加元素索引
                    $(".carousel-item.active .divRight").append($(ui.helper).attr({ "elem-index": elemIndex, "metro-type": "param" }).addClass("new").clone().draggable());


                    var paramId = $(ui.helper).parent("li.jstree-node").attr("id"); //参数ID
                    var paramName = $(ui.helper).text(); //参数名称


                    //新增系统磁贴表
                    var dataObj = { metroType: 0, paramId: paramId, paramName: paramName, elemIndex: elemIndex, x: x, y: y };
                    addSystemMetro($(this),dataObj);

                    //拖拽之后的元素（必须要添加二次拖拽事件，否则无效）
                    $(".carousel-inner a.jstree-anchor.new").draggable({

                    }).on('dragstart', function (e, ui) { //拖拽元素绑定事件

                    }).on('dragstop', function (e, ui) {  //拖拽元素停止事件

                        var x = ui.position.left; //x轴
                        var y = ui.position.top;  //y轴
                        var elemId = $(this).attr("elem-id"); //元素ID
                        if (!$.isEmpty(elemId)) {
                            updateMetroPoint($(this), elemId, x, y, metroType); //修改磁贴坐标
                        }
                    });
                });
                break;
            case "param":
                $(".carousel-inner .jstree-anchor").draggable({

                }).on('dragstart', function (e, ui) { //拖拽元素绑定事件
                    $(ui.helper).css("z-index", "9");
                }).on('dragstop', function (e, ui) {  //拖拽元素停止事件
                    var x = ui.position.left; //x轴
                    var y = ui.position.top;  //y轴
                    var elemId = $(this).attr("elem-id"); //元素ID
                    if (!$.isEmpty(elemId)) {
                        updateMetroPoint($(this), elemId, x, y, metroType); //修改磁贴坐标
                    }
                });
                break;
            case "chart":
                {
                    $(".carousel-inner .parameter-energy").draggable({

                    }).on('dragstart', function (e, ui) { //拖拽元素绑定事件
                        $(ui.helper).css("z-index", "9");
                    }).on('dragstop', function (e, ui) {  //拖拽元素停止事件
                        let x = ui.position.left; //X轴
                        let y = ui.position.top;  //Y轴
                        let elemId = $(this).attr("elem-id"); //元素ID
                        if (!$.isEmpty(elemId)) {
                            updateMetroPoint($(this), elemId, x, y, metroType); //修改磁贴坐标
                        }
                    });
                    $(".carousel-inner .parameter-energy").resizable({
                        //maxHeight: 250,
                        //maxWidth: 350,
                        minWidth: 150,  //最小宽度
                        minHeight: 150, //最小高度
                        start: function (event, ui) {
                        },
                        stop: function (event, ui) {
                            let resizeW = ui.size.width;  //调整宽度
                            let resizeH = ui.size.height; //调整高度
                            let left = ui.originalPosition.left;//左边缘位置
                            let top = ui.originalPosition.top;  //上边缘位置

                            let the = $(ui.helper);             //拖拽元素
                            let elemId = the.attr("elem-id");   //拖拽元素ID
                            let pageW = the.attr("elem-pageW"); //页面宽度
                            let pageH = the.attr("elem-pageH"); //页面高度

                            //Json配置数据
                            let jsonConfig = { 
                                pageW: pageW,
                                pageH: pageH,
                                chartMetro: {
                                    width: resizeW,
                                    height: resizeH
                                }
                            };
                            
                            //修改磁贴数据
                            $.reqAjax({
                                url: "/api/Cdc_SystemItemMetro/UpdatePoint?id=" + elemId + "&x=" + left + "&y=" + top + "&jsonConfig=" + JSON.stringify(jsonConfig) + "",
                                headers: { "token": true },
                                success: function (res) {
                                    console.log("结果=" + JSON.stringify(res));
                                }
                            });
                        },
                        resize: function (event, ui) {
                            let resizeW = ui.size.width;  //调整宽度
                            let resizeH = ui.size.height; //调整高度
                            let chartW = resizeW - 30;    //图表宽度
                            let chartH = resizeH - 59;    //图表高度
                            let elemId = $(ui.helper).attr("elem-id"); //拖拽元素ID

                            //调整图表尺寸
                            let metroChart = echarts.init(document.getElementById("chart-" + elemId));
                            metroChart.resize({
                                width: chartW,
                                height: chartH
                            });
                        }
                    });
                    break;
                }  
            case "new":
                {
                    $(".carousel-inner .information-news").draggable({

                    }).on('dragstart', function (e, ui) { //拖拽元素绑定事件
                        $(ui.helper).css("z-index", "9");
                    }).on('dragstop', function (e, ui) {  //拖拽元素停止事件
                        let x = ui.position.left; //X轴
                        let y = ui.position.top;  //Y轴
                        let elemId = $(this).attr("elem-id"); //元素ID
                     
                      
                       
                        if (!$.isEmpty(elemId)) {
                            updateMetroPoint($(this), elemId, x, y, metroType); //修改磁贴坐标
                        } else {
                            
                           
                        }
                        
                    });
                  
                    break;
                }  
        }
    };

    //添加动态元素拖拽
    window.addDynElemDrag = function (metroType, elemId) {
        switch (metroType) {
            case "metro-system":
                $(".metro-grid .metro-system[sys-id=" + elemId + "]").draggable({
                    helper: "clone" //复制元素
                }).on('dragstart', function (e, ui) { //拖拽元素绑定事件
                    $(ui.helper).css('z-index', '9');
                }).on('dragstop', function (e, ui) {  //拖拽元素停止事件

                    var x1 = ui.position.left; //x轴
                    var y1 = ui.position.top;  //y轴
                    var sysId = $(this).attr("sys-id"); //系统ID
                    var elemIndex = "system-" + $.getDate("yyyyMMddHHmmssfff");

                    //添加元素索引
                    $(".carousel-item.active .divRight").append($(ui.helper).attr("elem-index", elemIndex).addClass("new").clone().draggable());

                    //新增系统磁贴表
                    var dataObj = { metroType: 1, sysId: sysId, elemIndex: elemIndex, x: x1, y: y1 };
                    addSystemMetro($(this),dataObj);

                    //拖拽之后的元素（必须要添加二次拖拽事件，否则无效）
                    $(".carousel-inner .metro-system.new").draggable({

                    }).on('dragstart', function (e, ui) { //拖拽元素绑定事件

                    }).on('dragstop', function (e, ui) {  //拖拽元素停止事件

                        var x = ui.position.left; //x轴
                        var y = ui.position.top;  //y轴
                        var elemId = $(this).attr("elem-id"); //元素ID
                        if (!$.isEmpty(elemId)) {
                            updateMetroPoint($(this), elemId, x, y, metroType); //修改磁贴坐标
                        }
                    });

                });
                break;
        }
    };

    //移除磁贴
    $(".recycle-bin a").droppable({
        over: function (event, ui) {
            var elemId = $(ui.helper).attr("elem-id"); //系统项磁贴ID
            if (!$.isEmpty(elemId)) {
                //Ajax请求
                $.reqAjax({
                    url: "/api/Cdc_SystemItemMetro/Delete/" + elemId + "",
                    type: "delete",
                    headers: { "token": true },
                    success: function (res) {
                        $.layer.msg({ info: "移除成功", icon: "success", shift: 0 });
                    }
                })
            }

            ui.draggable.remove(); //移除开关元素
        }
    });

    //新增系统磁贴表
    var addSystemMetro = function (the, option) {
        var metroType = option.metroType; //磁铁类型
        var sysItemId = ""; //系统项ID
        $(".carousel-inner .carousel-item").each(function () {
            if ($(this).hasClass("active")) {
                sysItemId = $(this).attr("sys-item-id");
            }
        });
        let pageW = $(window).width();  //页面宽度
        let pageH = $(window).height(); //页面高度
        let jsonConfig = { //Json配置数据
            pageW: pageW,
            pageH: pageH
        };

        //请求数据
        var data = {
            MetroType: option.metroType,
            Cdc_SystemItemId: sysItemId,
            X: option.x,
            Y: option.y,
            JsonConfig: JSON.stringify(jsonConfig)
        };

        //检查磁铁类型
        switch (metroType) {
            case 0:
                data.Sys_ParameterId = option.paramId; //参数ID
                data.Name = option.paramName; //参数名称

                //默认磁铁参数样式
                //1：设置参数
                data.TextFontSize = 14;     //字体大小
                data.TextColor = "#E8F9B2"; //字体颜色
                data.IsShowName = true;     //是否显示参数名称

                //2：设置参数值
                data.FontSize = 14;         //字体大小
                data.DataColor = "#FFFFFF"; //字体颜色
                data.BackgroundColor = "#000000"; //背景颜色
                break;
            case 1:
                data.Cdc_SystemId = option.sysId; //系统ID
                break;
        }
        //Ajax请求
        $.reqAjax({
            url: "/api/Cdc_SystemItemMetro/Add",
            data: data,
            headers: { "token": true },
            success: function (res) {
                var data = res.Data;
                var id = data.Id; //添加成功后，返回的Id

                switch (metroType) {
                    case 0: //参数
                        var dataType = 0; //数据类型
                        var pValue = "";  //参数值
                        var pId = data.Sys_Parameter.Id;    //参数ID
                        var unit = data.Sys_Parameter.Unit; //单位
                        var viewData = data.ViewData;
                        if (!$.isEmpty(viewData)) {
                            pValue = viewData.Val;
                            dataType = viewData.DataType;
                        }
                        $(".carousel-inner a.jstree-anchor[elem-index=" + option.elemIndex + "]")
                            .attr({ "elem-id": id, "real-param-id": pId + dataType, "id": "" + pId + "_anchor", "elem-left": option.x, "elem-top": option.y, "elem-pageW": pageW, "elem-pageH": pageH })
                            .append("<div class=\"jstree-data\"><span><em class=\"value\">" + (pValue === null || pValue === "" ? "&nbsp" : pValue) + "</em> " + (unit === null || unit === "" ? "&nbsp" : unit) + "</span></div>");
                        break;
                    case 1: //系统
                        $(".carousel-inner .metro-system[elem-index=" + option.elemIndex + "]")
                            .attr({ "elem-id": id, "elem-left": option.x, "elem-top": option.y, "elem-pageW": pageW, "elem-pageH": pageH });
                        break;
                    case 8:
                        console.log($("[elem-index=" + option.elemIndex + "]").html())
                        $("[elem-index=" + option.elemIndex + "]").attr({ "elem-id": id, "elem-left": option.x, "elem-top": option.y, "elem-pageW": pageW, "elem-pageH": pageH }); 
                        break;
                }
            }
        });
    };

    //修改磁贴坐标
    var updateMetroPoint = function (the, elemId, x, y, metroType) {
        let pageW = $(window).width();  //页面宽度
        let pageH = $(window).height(); //页面高度
        let jsonConfig = { //Json配置数据
            pageW: pageW,   
            pageH: pageH
        };

        //检查磁贴类型
        switch (metroType) {
            case "device": //设备磁贴
                {
                    //添加Json配置数据(设备数量)
                    let deviceNumber = the.find(".number").html();
                    if ($.isEmpty(deviceNumber)) {
                        deviceNumber = "1";
                    }
                    jsonConfig.deviceNumber = deviceNumber;
                    break;
                }
        }

        $.reqAjax({
            url: "/api/Cdc_SystemItemMetro/UpdatePoint?id=" + elemId + "&x=" + x + "&y=" + y + "&jsonConfig=" + JSON.stringify(jsonConfig) + "",
            headers: { "token": true },
            success: function (res) {
                //alert("修改坐标成功");
                the.attr({ "elem-left": x, "elem-top": y, "elem-pageW": pageW, "elem-pageH": pageH }); //设置元素位置
            }
        });
    };

    //mouseover开关元素
    $(".carousel-inner").on("mouseover", ".divRight .metro-operation-status-col img", function (e) {
        var elemId = $(this).parents(".item.operation-status").attr("elem-id");
        var that = this;
        $.reqAjax({
            url: "/api/Cdc_SystemItemMetro/Find/" + elemId + "",
            type: "get",
            async: false,
            headers: { "token": true },
            success: function (res) {
                var data = res.Data;
                if (!$.isEmpty(res)) {
                    companyId = data.CompanyId;              //公司ID
                    paramId = data.Sys_Parameter["Name"];          //关联参数ID
                    
                    layer.tips(paramId, that, {
                        tips: 1,
                    });
                }
            }
        })
        

    });
    //mouseout开关元素
    $(".carousel-inner").on("mouseout", ".divRight .metro-operation-status-col img", function () {
        layer.closeAll('tips');
    });

    //点击开关元素
    $(".carousel-inner").on("click", ".divRight .metro-operation-status-col img", function () {
        var elemId = $(this).parents(".item.operation-status").attr("elem-id");          //元素ID
        var elemIndex = $(this).parents(".item.operation-status").attr("elem-index");    //元素索引值
        var x = $(this).parents(".item.operation-status").css("left").replace("px", ""); //元素X轴
        var y = $(this).parents(".item.operation-status").css("top").replace("px", "");  //元素Y轴
        let pageW = $(window).width();  //页面宽度
        let pageH = $(window).height(); //页面高度
        $.layer.open({ title: "修改开关", content: "switch/edit.html?elemId=" + elemId + "&elemIndex=" + elemIndex + "&x=" + x + "&y=" + y + "&pageW=" + pageW + "&pageH=" + pageH + "", width: "570px", height: "700px" });
    });

    //点击设备元素
    $(".carousel-inner").on("click", ".divRight .equipment-energy", function () {
        let elemId = $(this).attr("elem-id");          //元素ID
        let elemIndex = $(this).attr("elem-index");    //元素索引值
        let x = $(this).css("left").replace("px", ""); //元素X轴
        let y = $(this).css("top").replace("px", "");  //元素Y轴
        let pageW = $(window).width();  //页面宽度
        let pageH = $(window).height(); //页面高度
        $.layer.open({ title: "修改设备", content: "device/manage.html?oper=edit&elemId=" + elemId + "&elemIndex=" + elemIndex + "&x=" + x + "&y=" + y + "&pageW=" + pageW + "&pageH=" + pageH + "", width: "1000px", height: "750px" });
    });

    //点击系统元素
    $(".carousel-inner").on("click", ".divRight .metro-system", function () {
        var carData = $(this).attr("car-data"); //焦点图数据
        if (!$.isEmpty(carData)) {
            carData = eval('(' + carData + ')');
            getCarousel(carData, false); //获取系统焦点图的磁贴
        }
    });

    //点击参数值数据元素
    $(".carousel-inner").on("click", ".divRight .total-active-energy", function () {
        var elemId = $(this).attr("elem-id");          //元素ID
        var elemIndex = $(this).attr("elem-index");    //元素索引值
        let x = $(this).css("left").replace("px", ""); //元素X轴
        let y = $(this).css("top").replace("px", "");  //元素Y轴
        let pageW = $(window).width();  //页面宽度
        let pageH = $(window).height(); //页面高度
        $.layer.open({ title: "修改参数值", content: "metro/data.html?oper=edit&elemId=" + elemId + "&elemIndex=" + elemIndex + "&x=" + x + "&y=" + y + "&pageW=" + pageW + "&pageH=" + pageH + "", width: "650px", height: "750px" });
    });
    //点击配置视频元素
    $(".carousel-inner").on("click", ".divRight .video-text", function () {
        var elemId = $(this).attr("elem-id");          //元素ID
        var elemIndex = $(this).attr("elem-index");    //元素索引值
        let x = $(this).css("left").replace("px", ""); //元素X轴
        let y = $(this).css("top").replace("px", "");  //元素Y轴
        let pageW = $(window).width();  //页面宽度
        let pageH = $(window).height(); //页面高度
        $.layer.open({ title: "修改视频配置", content: "metro/videoConfig.html?oper=edit&elemId=" + elemId + "&elemIndex=" + elemIndex + "&x=" + x + "&y=" + y + "&pageW=" + pageW + "&pageH=" + pageH + "", width: "700px", height: "550px" });
    });
    //播放视频
    window.ezopenVideo = function (videoKey, pageW, pageH) {
        var domain = "https://open.ys7.com";
        var EZOPENDemo;
        const ezopenInit = () => {
            window.EZOPENDemo = new EZUIKit.EZUIKitPlayer({
                id: videoKey,
                width: pageW|| 500,
                height: pageH ||500,
                autoplay: false,
                template: "pcLive",
                url: "ezopen://PTs6aTfZ@open.ys7.com/AB5252977/1.live",
                accessToken: "at.49ae08i0bsuf5sn76gbishdm4ze7sff2-26kzdemo5q-1s5i328-fooq6ynw5",
             });
        }
        ezopenInit();
    }
    window.stopvideo = function (videoKey) {
        console.log('sadasdaddsad')
        $(".metro-card-data").delegate('#' + videoKey + '-wrap', "click", function () {
            event.stopPropagation();
            console.log('子元素被点击');
        });
    }
    //点击磁铁，修改自定义文字
    $(".carousel-inner").on("click", ".divRight .custom-text", function () {
        var elemId = $(this).attr("elem-id");          //元素ID
        var elemIndex = $(this).attr("elem-index");    //元素索引值
        let x = $(this).css("left").replace("px", ""); //元素X轴
        let y = $(this).css("top").replace("px", "");  //元素Y轴
        let pageW = $(window).width();  //页面宽度
        let pageH = $(window).height(); //页面高度
        $.layer.open({ title: "修改自定义文字", content: "metro/customText.html?oper=edit&elemId=" + elemId + "&elemIndex=" + elemIndex + "&x=" + x + "&y=" + y + "&pageW=" + pageW + "&pageH=" + pageH + "", width: "450px", height: "550px" });
    });

    //点击磁铁参数元素
    $(".carousel-inner").on("click", ".divRight .jstree-anchor", function () {
        var elemId = $(this).attr("elem-id"); //元素ID
        var paramId = $(this).attr("id");     //参数ID
        if (!$.isEmpty(paramId)) {
            paramId = paramId.split("_")[0];
        }

        let x = $(this).css("left").replace("px", ""); //元素X轴
        let y = $(this).css("top").replace("px", "");  //元素Y轴
        let pageW = $(window).width();  //页面宽度
        let pageH = $(window).height(); //页面高度
        $.layer.open({ title: "修改参数属性", content: "metro/param.html?elemId=" + elemId + "&paramId=" + paramId + "&x=" + x + "&y=" + y + "&pageW=" + pageW + "&pageH=" + pageH + "", width: "500px", height: "600px" });
    });

    let framSrc;
    var timer;
    var isMouseOut;
    //mouseover折线图
    $(".carousel-inner").on("mouseover", ".divRight .jstree-data", function () {
        var elemId = $(this).parent().attr("elem-id");
        var paramId = $(this).parent().attr("id");
        isMouseOut = new Date().getTime();
        var case1 = isMouseOut;
        if (!$.isEmpty(paramId)) {
            paramId = paramId.split("_")[0];
        }
        framSrc = "metro/zhexian.html?elemId=" + elemId + "&paramId=" + paramId;
        timer = setTimeout(function () {
            if (case1 == isMouseOut) {
                $.layer.open({
                    title: "最近一日数据查询",
                    content: framSrc,
                    width: "600px",
                    height: "400px"
                });
            }
        }, 2500); // 延时
        $(this).data("timer", timer);
    }).on("mouseout", ".divRight .jstree-data", function () {
        clearTimeout($(this).data("timer"));
    });


    ////mouseleave折线图

    $("body").on("mouseleave", ".layui-layer.layui-layer-iframe.layui-layer-demo",
        function () {
            clearTimeout(timer)
            // 获取所有的 iframe 元素
            var iframes = document.getElementsByTagName('iframe');
            // 遍历所有的 iframe 元素
            for (var i = 0; i < iframes.length; i++) {
                var iframe = iframes[i];
                // 判断当前的 iframe 元素的 src 属性是否符合条件
                if (iframe.src.indexOf(framSrc) != -1 && framSrc) {
                    $(".layui-layer.layui-layer-iframe.layui-layer-demo").remove();
                    $(".layui-layer-shade").remove();
                    framSrc = undefined;
                }
            }
        }
    ) 
    //点击磁铁图表元素
    $(".carousel-inner").on("click", ".parameter-energy", function () {
        var elemId = $(this).attr("elem-id");          //元素ID
        var elemIndex = $(this).attr("elem-index");    //元素索引值
        var x = $(this).css("left").replace("px", ""); //元素X轴
        var y = $(this).css("top").replace("px", "");  //元素Y轴
        let pageW = $(window).width();  //页面宽度
        let pageH = $(window).height(); //页面高度
        $.layer.open({ title: "修改磁贴数据", content: "metro/chart.html?oper=edit&elemId=" + elemId + "&elemIndex=" + elemIndex + "&x=" + x + "&y=" + y + "&pageW=" + pageW + "&pageH=" + pageH + "", width: "650px", height: "750px" });
    });

    //点击磁铁图表元素(时间组)
    $(".carousel-inner").on("click", ".parameter-energy .metro-card-title .time-group a", function (e) {
        e.stopPropagation(); //阻止事件（点击磁铁图表元素）
        if (!$(this).hasClass("on")) {
            $(this).addClass("on").siblings("a").removeClass("on");

            var bgColor = $(this).parent(".parameter-energy").css("background"); //背景颜色
            var chartId = $(this).attr("chart-id");   //图表ID
            var paramIds = $(this).attr("param-ids"); //参数ID

            var sTime = "";    //开始时间
            var eTime = "";    //结束时间
            var dataType = ""; //数据类型
            var timeType = $(this).attr("time-type"); //时间类型
            switch (timeType) {
                case "hour": //小时
                    var timeH = $.getDate("yyyy-MM-dd HH");
                    sTime = timeH + ":00:00";
                    eTime = timeH + ":59:59";
                    dataType = "1";
                    break;
                case "day": //日
                    var timeD = $.getDate("yyyy-MM-dd");
                    sTime = timeD + " 00:00:00";
                    eTime = timeD + " 23:59:59";
                    dataType = "2";
                    break;
            }

            metroChartData(dataType, sTime, eTime, chartId, paramIds, bgColor);
        }
    });


    
    //磁贴图表渲染
    window.renderMetroChart = function (elemId, data, bgColor) {
        var color_index = -1;
        const color_1 = ["#9EFFFF", '#73DD39', '#FF69B4', '#FFFF00', '#73A0FA', '#CCCC00', '#666600', '#FF6666', '#009933','#0066CC'];
        const color_2 = ["#9E87FF", '#73DDFF', '#FE9A8B', '#F5B400', '#73DEB3', '#FFCC33', '#99CC99', '#FFCCCC', '#99CC00','#336699']; //蓝绿色|蓝紫色|绿色|粉红色|黄色
        const shadowColor = [ //对应color_2主色的rgba颜色
            "rgba(158, 135, 255, 0.3)",
            "rgba(115, 221, 255, 0.3)",
            "rgba(254, 154, 139, 0.3)",
            "rgba(245, 180, 0, 0.3)",
            "rgba(115, 222, 179, 0.3)",
            "rgba(255, 204, 51, 0.3)",
            "rgba(153, 204, 153, 0.3)",
            "rgba(255, 204, 204, 0.3)",
            "rgba(153, 204, 0, 0.3)",
            "rgba(51, 102, 153, 0.3)"
        ]; 

        //时间数据
        var arrTime = [];
        var arrX = data.XAxisData;
        if (arrX.length > 0) {
            for (var i = 0; i < arrX.length; i++) {
                arrTime.push($.getDate("HH:mm", arrX[i]));
            }
        }

        //Y轴数据
        var yAxis = [];
        var series = [];
        var arrY = data.YAxisDatas;
        if (arrY.length > 0) {
            for (var i = 0; i < arrY.length; i++) {

                var yAxisIndex = i;

                //组装yAxis
                var position = "left";
                if (i > 0) {
                    position = "right";
                }
                var unit = arrY[i].Y.Unit; //单位
                var yAxisObj =
                {
                    position: position,
                    name: unit,
                    type: 'value',
                    axisTick: { show: false },
                    axisLabel: { color: '#ffffff', fontSize: 12 },
                    splitLine: { show: false },
                    axisLine: {
                        show: true,
                        lineStyle: {
                            color: '#ffffff'
                        }
                    }
                };
                yAxis.push(yAxisObj);

                //组装series
                var seriess = arrY[i].Seriess;
                if (seriess.length > 0) {
                    for (var j = 0; j < seriess.length; j++) {
                        color_index++;
                        var name = seriess[j].Name;  //名称
                        var datas = seriess[j].Datas; //数据
                        var seriesObj = {
                            name: name,
                            type: 'line',
                            data: datas,
                            symbolSize: 1,
                            symbol: 'circle',
                            smooth: true,
                            yAxisIndex: yAxisIndex,
                            showSymbol: true,
                            lineStyle: {
                                width: 2,
                                color: new echarts.graphic.LinearGradient(0, 1, 0, 0, [
                                    { offset: 0, color: color_1[color_index] },
                                    { offset: 1, color: color_2[color_index] }
                                ]),
                                shadowColor: shadowColor[color_index],
                                shadowBlur: 10,
                                shadowOffsetY: 20
                            },
                            itemStyle: {
                                color: color_2[color_index],
                                borderColor: color_2[color_index]
                            }
                        };

                        series.push(seriesObj);
                    }
                }
            }
        }

        //显示图表
        let metroChart = echarts.init(document.getElementById(elemId));
        metroChart.clear(); //重绘之前,先清除一下（否则没有动画效果和清除图表操作）

        if (arrTime.length > 0 && yAxis.length > 0 && series.length > 0) {
            var option = {
                backgroundColor: bgColor,
                legend: {
                    icon: 'circle',
                    x: 'center',
                    top: '10',
                    itemWidth: 6,
                    itemGap: 10,
                    textStyle: {
                        fontSize: 12,   //字体大小
                        color: '#ffffff'//字体颜色
                    }
                },
                tooltip: {
                    trigger: 'axis',
                    axisPointer: {
                        label: {
                            show: true,
                            backgroundColor: '#fff',
                            color: '#556677',
                            borderColor: 'rgba(0,0,0,0)',
                            shadowColor: 'rgba(0,0,0,0)',
                            shadowOffsetY: 0
                        },
                        lineStyle: {
                            width: 0
                        }
                    },
                    backgroundColor: '#fff',
                    color: '#5c6c7c',
                    padding: [10, 10],
                    extraCssText: 'box-shadow: 1px 0 2px 0 rgba(163,163,163,0.5)'
                },
                grid: {
                    left: "15",
                    right: "10",
                    bottom: "0",
                    top: "65",
                    containLabel: true
                },
                xAxis: [{
                    type: 'category',
                    data: arrTime,
                    axisLabel: {//x轴文字的配置
                        show: true,
                        textStyle: {
                            color: "#ffffff",
                            fontSize: 12
                        }
                    },
                    axisLine: {
                        lineStyle: {
                            color: '#fff'
                        }
                    },
                    axisTick: {
                        show: false
                    },
                    axisPointer: {
                        label: {
                            margin: 15,
                            fontSize: 12, //移入时的字体大小
                            backgroundColor: {
                                type: 'linear',
                                x: 0,
                                y: 0,
                                x2: 0,
                                y2: 1,
                                colorStops: [{
                                    offset: 0,
                                    color: '#fff' // 0% 处的颜色
                                }, {
                                    // offset: 0.9,
                                    offset: 0.86,

                                    color: '#fff' // 0% 处的颜色
                                }, {
                                    offset: 0.86,
                                    color: '#33c0cd' // 0% 处的颜色
                                }, {
                                    offset: 1,
                                    color: '#33c0cd' // 100% 处的颜色
                                }],
                                global: false // 缺省为 false
                            }
                        }
                    },
                    boundaryGap: false
                }],
                yAxis: yAxis,
                series: series
            };

            metroChart.setOption(option);

            window.onresize = function () {
                metroChart.resize();
            }
        }
    };

    //磁贴图表数据
    var metroChartData = function (dataType, sTime, eTime, chartId, paramIds, bgColor) {
        $.reqAjax({
            url: "/api/QueryData/GetParameterDatas?dataType=" + dataType + "&stime=" + sTime + "&etime=" + eTime + "&ids=" + paramIds + "",
            type: "get",
            headers: { "token": true },
            success: function (res) {
                var data = res.Data;
                if (!$.isEmpty(data)) {
                    renderMetroChart(chartId, data, bgColor);
                }
            }
        })
    };

    

    //////////////////////////////////////////头部菜单//////////////////////////////////////////
    //系统管理
    $(".navbar-nav .create-new-system").on("click", function () {
        $.layer.open({ title: "系统管理", content: "sys/list.html", width: "505px", height: "330px" });
    });

    $(".carousel-inner ").on("click", 'a.nav-link', function () {
        openNews();
    });

    //磁贴管理
    $(".navbar-nav .add-metro").on("click", function () {
        document.getElementById("metro-main").style.width = "495px";
        document.getElementById("notice-sidebar").style.width = "0px";
        document.getElementById("parameter-sidebar").style.width = "0px";
    });

    //关闭功能磁贴
    $(".metro-main .metro-header .metro-close-icon").on("click", function () {
        document.getElementById("metro-main").style.width = "0";
    });

    //关闭系统参数
    $("#parameter-sidebar .metro-header a").on("click", function () {
        document.getElementById("parameter-sidebar").style.width = "0";
    });


    //////////////////////////////////////////关闭侧边栏///////////////////////////////////////
    //关闭消息通知
    $("#notice-sidebar .metro-header a").on("click", function () {
        document.getElementById("notice-sidebar").style.width = "0";
    });

    //////////////////////////////////////////数据中心/////////////////////////////////////////
    //数据中心点击事件
    $(".navbar-nav .data-center").on("click", function () {
        $("body").addClass("data-center-show"); //显示页面
        getDcParamTree(); //获取数据中心参数树形
        getDcChartData(); //数据中心图表数据
    });

    //获取数据中心参数树形
    window.getDcParamTree = function (flag) {
        $.reqAjax({
            url: "api/Sys_Area/GetTree",
            type: "get",
            async: false,
            headers: { "token": true },
            success: function (res) {
                var data = res.Data;
                if (data.length > 0) {
                    for (var i = 0; i < data.length; i++) {
                        var id = data[i].id; //参数ID
                        var parent = data[i].parent === "#" ? 0 : data[i].parent; //父级ID
                        var type = data[i].type; //参数类型
                        if (type !== -1 && searchDc.paramIds === "") {
                            searchDc.paramIds = id; //获取第一项数据ID
                        }
                        switch (type) {
                            case -1: //区域
                                data[i].icon = "../images/jstree/p1.png";
                                break;
                            case 0: //参数
                                data[i].icon = "../images/jstree/p2.png";
                                break;
                            case 1: //价格
                                data[i].icon = "../images/jstree/p4.png";
                                break;
                            case 2: //虚拟参数
                                data[i].icon = "../images/jstree/p3.png";
                                break;
                        }

                        data[i].state = { "opened": true }; //节点展开状态
                        data[i].a_attr = { "data-type": type, "parent-id": parent }; //节点自定义属性值
                    }

                    var $tree = $(".data-center-main .metro-tree"); //树形对象

                    //新增区域成功后回调
                    if (flag === "area" || flag === "virtualParam" || flag === "priceParam" || flag === "deleteNode" || flag === "refresh") {
                        //$tree.data('jstree', false).empty(); //清空树实例,否则无法显示最新数据
                        //$tree.jstree(true).refresh(); //刷新树节点
                        $tree.jstree(true).destroy(); //清除树节点
                    }
                   

                    //树形数据绑定
                    $tree.jstree({
                        "core": {
                            "data": data,
                            "check_callback": true,
                            "multiple": true,
                        }
                    });


                    //树形完成加载后回调
                    $tree.on('ready.jstree', function () {

                        //默认选中第一个节点
                        $tree.jstree('select_node', searchDc.paramIds, true);

                        //添加操作元素
                        $tree.find("a.jstree-anchor").each(function () {
                            $(this).append("<span class=\"fa fa-pencil mr-3 edit\"></span><span class=\"fa fa-trash-o delete\"></span>");
                        });

                        //添加鼠标移入移出事件
                        $tree.find("a.jstree-anchor").hover(function () {
                            let isShow = true; //是否显示
                            let dataType = $(this).attr("data-type"); //参数类型
                            if (dataType === "-1") {
                                //检查该区域下是否存在系统默认参数,如果存在则不显示区域操作图标
                                $(this).siblings("ul.jstree-children").find("a.jstree-anchor").each(function () {
                                    var value = $(this).attr("data-type");
                                    if (value === "0") {
                                        isShow = false;
                                    }
                                });
                            }
                            if (dataType === "0") {
                                isShow = false;
                            }

                            //检查是否显示
                            if (isShow) {
                                $(this).children("span.fa").stop(true, true).show(); //显示图标
                            }
                        }, function () {
                            $(this).children("span.fa").stop(false, false).hide(); //隐藏图标
                        });

                        //添加拖拽元素到指定区域内触发
                        $("#dcChart").droppable({
                            accept: $tree.find("a.jstree-anchor"),
                            drop: function (event, ui) {
                                var type = $(ui.helper).attr("data-type"); //参数类型
                                if (type !== "-1") {
                                    var id = $(ui.helper).parent("li.jstree-node").attr("id"); //参数ID
                                    var strIds = searchDc.paramIds + "," + id; //拼接ID

                                    searchDc.paramIds = strIds;
                                    getDcChartData();
                                }
                            }
                        });

                        //添加元素拖拽
                        var dragObj = null; //拖拽元素对象
                        $tree.find("a.jstree-anchor").draggable({
                            helper: "clone" //复制元素
                        }).on('drag', function (e, ui) { //拖拽元素过程中事件
                        }).on('dragstart', function (e, ui) { //拖拽元素开始事件
                            dragObj = $(this); //拖拽元素对象
                        }).on('dragstop', function (e, ui) { //拖拽元素停止事件
                            //alert("a1");
                        });

                        //拖拽元素移到指定区域内事件
                        $tree.find("a.jstree-anchor").droppable({
                            drop: function (event, ui) {

                                //获取拖拽元素
                                var dragId = dragObj.parent("li.jstree-node").attr("id"); //拖拽元素参数ID                              
                                var dragPrevId = dragObj.parent("li.jstree-node").prev().attr("id"); //拖拽元素相邻上一个同级元素参数ID
                                var dragParentId = dragObj.attr("parent-id"); //拖拽元素参数父级ID
                                var dragType = dragObj.attr("data-type"); //拖拽元素参数类型
                                var dragHtml = dragObj.parent("li.jstree-node").prop("outerHTML"); //拖拽元素Html

                                //获取目标元素
                                var dropId = $(this).parent("li.jstree-node").attr("id"); //目标元素参数ID
                                var dropParentId = $(this).attr("parent-id"); //目标元素参数父级ID
                                var dropType = $(this).attr("data-type");     //目标元素参数类型

                                //获取请求数据
                                var reqData = {
                                    parent: {},
                                    sorts: []
                                };

                                //交换元素位置
                                if (dragType === "-1") {
                                    var childIds = []; //拖拽元素所有子级ID

                                    //获取拖拽元素本身所有下级ID
                                    dragObj.parent("li.jstree-node").find("li.jstree-node").each(function () {
                                        var id = $(this).attr("id");
                                        childIds.push(id);
                                    });

                                    //检查元素是否可以拖拽
                                    if (childIds.indexOf(dropId) !== -1) {
                                        return false;
                                    } else {
                                        //第一种：移到区域下面
                                        var ul_1 = $(this).siblings("ul.jstree-children"); //判断元素是否存在
                                        if (ul_1.length > 0) {
                                            $(this).siblings(ul_1).children().first().before(dragHtml); //将拖拽元素放置在目标元素的后面
                                        } else {
                                            $(this).parent("li.jstree-node").removeClass("jstree-leaf").addClass("jstree-open");
                                            $(this).after("<ul role=\"group\" class=\"jstree-children\">" + dragHtml + "<ul/>");

                                            //添加展开收缩事件(拖拽元素后自带的事件失效)
                                            //$(this).siblings("i.jstree-icon").on("click", function () {
                                            //    var open = $(this).parent("li.jstree-node").hasClass("jstree-open"); //节点是否展开
                                            //    if (open) {
                                            //        $(this).parent("li.jstree-node").addClass("jstree-closed").removeClass("jstree-open");
                                            //        $(this).siblings("ul.jstree-children").hide(200);
                                            //    } else {
                                            //        $(this).parent("li.jstree-node").addClass("jstree-open").removeClass("jstree-closed");
                                            //        $(this).siblings("ul.jstree-children").show(200);
                                            //    }
                                            //});
                                        }

                                        dragObj.parent("li.jstree-node").remove(); //移除拖拽元素
                                        $tree.find("li.jstree-node[id=" + dragId + "] .ui-draggable-dragging").remove(); //移除拖拽之后的多余元素


                                        ////////////////////获取数据(将拖拽元素移到区域)////////////////////
                                        reqData.parent.id = dragId;     //拖拽元素参数ID 
                                        reqData.parent.parent = dropId; //目标元素参数ID

                                        //获取目标元素所有父级
                                        var temp = [];
                                        var getAllParent = function (data, dropId) {
                                            for (var i = 0; i < data.length; i++) {
                                                var item = data[i];
                                                if (item.id === dropId) {
                                                    temp.push(item.id);
                                                    getAllParent(data, item.parent);
                                                }
                                            }
                                        }
                                        getAllParent(data, dropId);

                                        //获取最上层父级ID
                                        if (temp.length > 0) {
                                            var topId = temp[temp.length - 1]
                                            if (!$.isEmpty(topId)) {

                                                //遍历最上层父级ID下所有的节点
                                                $tree.find("li.jstree-node[id=" + topId + "] a.jstree-anchor").each(function () {
                                                    i = i + 1; //排序值
                                                    var id = $(this).parent("li.jstree-node").attr("id"); //参数ID
                                                    reqData.sorts.push({ id: id, sort: i });
                                                });

                                            }
                                        }
                                    }

                                } else {

                                    if (dropType === "-1") {
                                        var ul_2 = $(this).siblings("ul.jstree-children"); //判断元素是否存在
                                        if (ul_2.length > 0) {
                                            $(this).siblings(ul_2).children().first().before(dragHtml);
                                        } else {
                                            $(this).parent("li.jstree-node").removeClass("jstree-leaf").addClass("jstree-open");
                                            $(this).after("<ul role=\"group\" class=\"jstree-children\">" + dragHtml + "<ul/>");

                                            //添加展开收缩事件(拖拽元素后自带的事件失效)
                                            //$(this).siblings("i.jstree-icon").on("click", function () {
                                            //    var open = $(this).parent("li.jstree-node").hasClass("jstree-open"); //节点是否展开
                                            //    if (open) {
                                            //        $(this).parent("li.jstree-node").addClass("jstree-closed").removeClass("jstree-open");
                                            //        $(this).siblings("ul.jstree-children").hide(200);
                                            //    } else {
                                            //        $(this).parent("li.jstree-node").addClass("jstree-open").removeClass("jstree-closed");
                                            //        $(this).siblings("ul.jstree-children").show(200);
                                            //    }
                                            //});
                                        }

                                        dragObj.parent("li.jstree-node").remove(); //移除拖拽元素
                                        $tree.find("li.jstree-node[id=" + dragId + "] .ui-draggable-dragging").remove(); //移除拖拽之后的多余元素


                                        ////////////////////获取数据(将拖拽元素移到区域)////////////////////
                                        reqData.parent.id = dragId;     //拖拽元素参数ID 
                                        reqData.parent.parent = dropId; //目标元素参数ID

                                        //遍历节点数据
                                        $(this).siblings("ul.jstree-children").children("li.jstree-node").each(function (i) {
                                            i = i + 1; //排序值
                                            var id = $(this).attr("id"); //参数ID
                                            reqData.sorts.push({ id: id, sort: i });
                                        });

                                    }
                                    else {

                                        if (dropId === dragPrevId) {
                                            $(this).parent("li.jstree-node").before(dragHtml); //将拖拽元素放置在目标元素的前面
                                        } else {
                                            $(this).parent("li.jstree-node").after(dragHtml); //将拖拽元素放置在目标元素的后面
                                        }

                                        dragObj.parent("li.jstree-node").remove(); //移除拖拽元素
                                        $tree.find("li.jstree-node[id=" + dragId + "] .ui-draggable-dragging").remove(); //移除拖拽之后的多余元素


                                        ////////////////////获取数据(将拖拽元素移到节点)////////////////////
                                        reqData.parent.id = dragId;           //拖拽元素参数ID 
                                        reqData.parent.parent = dropParentId; //目标元素参数父级ID

                                        //遍历节点数据
                                        $(this).parent("li.jstree-node").parent("ul.jstree-children").children("li.jstree-node").each(function (i) {
                                            i = i + 1; //排序值
                                            var id = $(this).attr("id"); //参数ID
                                            reqData.sorts.push({ id: id, sort: i });
                                        });
                                    }
                                }

                                ////////////////修改树形数据////////////////

                                $.reqAjax({
                                    url: "/api/Sys_Area/updatenode",
                                    data: reqData,
                                    headers: { "token": true },
                                    success: function (res) {
                                        getDcParamTree("refresh");
                                        //$.layer.msg({ info: info, icon: "success", shift: 0 });
                                        //$.layer.close(); //关闭层
                                    }
                                })
                            }
                        });
                    });
                }
            },
            complete: function () {

            }
        })
    }

    //树形菜单点击事件
    $(".data-center-main .metro-tree").on("click", ".jstree-node .jstree-anchor", function (e) {
        if (e.target === e.currentTarget) { //防止父元素覆盖资源的绑定事件操作
            var id = $(this).parent("li.jstree-node").attr("id"); //节点ID
            searchDc.paramIds = id;

            var navIndex = $(".data-center-main .data-center-tabs .nav-item.active").index(); //当前选中的选项卡索引值
            switch (navIndex) {
                case 0: //数据图表
                    getDcChartData();
                    break;
                case 1: //日历图表
                    renderDcCalendar();  //渲染日历
                    getDcCalendarData(); //获取日历数据
                    break;
            }
        }
    });

    //树形菜单修改事件
    $(".data-center-main .metro-tree").on("click", ".jstree-node span.edit", function (e) {
        e.stopPropagation(); //阻止事件冒泡

        var paramId = $(this).parents("li.jstree-node").attr("id");         //参数ID
        var parentId = $(this).parent("a.jstree-anchor").attr("parent-id"); //父级ID
        var type = $(this).parent("a.jstree-anchor").attr("data-type");     //参数类型
        switch (type) {
            case "-1": //区域参数
                $.layer.open({ title: "修改区域", content: "dc/area.html?oper=edit&paramId=" + paramId + "&parentId=" + parentId + "", width: "600px", height: "550px" });
                break;
            case "2":  //虚拟参数
                $.layer.open({ title: "修改虚拟参数", content: "dc/virtualParam.html?oper=edit&paramId=" + paramId + "&parentId=" + parentId + "", width: "600px", height: "550px" });
                break;
            case "1":  //价格参数
                $.layer.open({ title: "修改价格参数", content: "dc/priceParam.html?oper=edit&paramId=" + paramId + "&parentId=" + parentId + "", width: "600px", height: "700px" });
                break;
        }
    });

    //树形菜单删除事件
    $(".data-center-main .metro-tree").on("click", ".jstree-node span.delete", function (e) {
        e.stopPropagation(); //阻止事件冒泡

        var reqUrl = "";
        var paramId = $(this).parents("li.jstree-node").attr("id"); //参数ID
        var type = $(this).parent("a.jstree-anchor").attr("data-type"); //参数类型
        if (type === "-1") {
            reqUrl = "/api/Sys_Area/Delete/" + paramId + ""; //删除区域
        } else {
            reqUrl = "/api/Sys_Parameter/Delete/" + paramId + ""; //删除参数
        }

        $.layer.confirm({
            content: '您确定要删除该数据吗？', icon: 'quest', ok: function () {
                $.reqAjax({
                    url: reqUrl,
                    type: "delete",
                    headers: { "token": true },
                    success: function (res) {
                        $.layer.msg({ info: "删除成功", icon: "success", shift: 0 });
                        getDcParamTree("deleteNode");
                    }
                })
            }
        });
    });

    //时间选择器
    $(".datetime-picker").datetimepicker({
        language: 'zh-CN',//语言
        autoclose: true,  //选择后自动关闭
        clearBtn: true,   //清除按钮
        //todayBtn: true, //今天按钮
        todayHighlight: true,//当日高亮自动定位到当日
        format: 'yyyy-mm-dd hh:ii' //时间格式
    });

    //时间选项卡切换
    $(".data-center-main .data-center-tabs .nav-pills .nav-item a").on("click", function () {
        var isActive = $(this).hasClass("active"); //是否选中
        if (!isActive) {
            var timeChangeType = $(this).attr("time-change-type"); //切换类型
            switch (timeChangeType) {
                case "genQuery": //常规查询
                    var isQuery = searchDc.isQuery; //是否查询
                    var timeType = $(".data-center-main .btn-group .btn.active").attr("time-type");
                    if (!$.isEmpty(timeType) && isQuery) {
                        searchDc.timeType = timeType;
                        getDcChartData(); //数据中心图表数据
                    }
                    break;
                case "advQuery": //高级查询
                    var sTime = $(".data-center-main #startTime").val(); //开始时间
                    var eTime = $(".data-center-main #endTime").val();   //结束时间
                    if (!$.isEmpty(sTime) && !$.isEmpty(eTime)) {
                        searchDc.sTime = sTime + ":00";
                        searchDc.eTime = eTime + ":59";
                        searchDc.timeType = "timeInterval";
                        getDcChartData();
                    }
                    break;
            }
        }
    });

    //时间按钮组
    $(".data-center-main .btn-group .btn").on("click", function () {
        var isActive = $(this).hasClass("active"); //是否选中
        if (!isActive) {
            var timeType = $(this).attr("time-type"); //时间类型
            if (!$.isEmpty(timeType)) {
                searchDc.timeType = timeType;

                getDcChartData(); //数据中心图表数据
            }
        }

        $(this).addClass("active").siblings(".btn").removeClass("active");

    });

    //查询
    $(".data-center-main #btnSearch").on("click", function () {
        var sTime = $(".data-center-main #startTime").val(); //开始时间
        var eTime = $(".data-center-main #endTime").val();   //结束时间
        if ($.isEmpty(sTime) && $.isEmpty(eTime)) {
            $.layer.msg({ info: "请选择开始时间与结束时间", icon: "warn" });
            return;
        }
        if ($.isEmpty(sTime)) {
            $.layer.msg({ info: "请选择开始时间", icon: "warn" });
            return;
        }
        if ($.isEmpty(eTime)) {
            $.layer.msg({ info: "请选择结束时间", icon: "warn" });
            return;
        }

        searchDc.sTime = sTime + ":00";
        searchDc.eTime = eTime + ":59";
        searchDc.timeType = "timeInterval";
        getDcChartData();
        searchDc.isQuery = true; //标识是否查询
    });

    //价格管理
    $(".data-center-main #btnPriceManage").on("click", function () {
        $.layer.open({ title: "价格管理", content: "price/list.html", width: "600px", height: "600px" });
    });

    //新增区域
    $(".data-center-main .dropdown-menu .add-area").on("click", function () {
        $.layer.open({ title: "新增区域", content: "dc/area.html?oper=add", width: "600px", height: "550px" });
    });

    //新增虚拟参数
    $(".data-center-main .dropdown-menu .add-virtual-param").on("click", function () {
        var data = $(".data-center-main .metro-tree").jstree(true).get_selected(true); //获取选中节点的数据对象
        var paramId = data[0].original.id; //参数ID
        var type = data[0].original.type;  //参数类型
        if (type !== -1) {
            $.layer.msg({ info: "请选择区域参数", icon: "warn" });
            return;
        }
        $.layer.open({ title: "新增虚拟参数", content: "dc/virtualParam.html?oper=add&paramId=" + paramId + "", width: "600px", height: "550px" });
    });

    //新增价格参数
    $(".data-center-main .dropdown-menu .add-price-param").on("click", function () {
        var data = $(".data-center-main .metro-tree").jstree(true).get_selected(true); //获取选中节点的数据对象
        var paramId = data[0].original.id; //参数ID
        var type = data[0].original.type;  //参数类型
        if (type !== -1) {
            $.layer.msg({ info: "请选择区域参数", icon: "warn" });
            return;
        }
        $.layer.open({ title: "新增价格参数", content: "dc/priceParam.html?oper=add&paramId=" + paramId + "", width: "600px", height: "700px" });
    });

    //选项卡切换
    $(".data-center-main .data-center-tabs .nav-tabs .nav-item").on("click", function () {
        var index = $(this).index();
        switch (index) {
            case 0:
                getDcChartData();
                break;
            case 1:
                renderDcCalendar();  //渲染日历
                getDcCalendarData(); //获取日历数据
                break;
        }
    });


   
    //渲染数据图表
    var renderDcChart = function (timeType, data) {

        //图例数据
        var legData = data.LegendData;

        //X轴数据解析
        var arrX = data.XAxisData;
        var arrTime = []; //时间
        switch (timeType) {
            case "hour": //时
                //if (arrX.length > 0) {
                //    for (var a = 0; a < arrX.length; a++) {
                //        arrTime.push($.getDate("HH:mm", arrX[a]));
                //    }
                //}
                break;
            case "day": //日
                //if (arrX.length > 0) {
                //    for (var b = 0; b < arrX.length; b++) {
                //        arrTime.push($.getDate("HH:mm", arrX[b]));
                //    }
                //}
                break;
            case "week":
                //if (arrX.length > 0) {
                //    for (var c = 0; c < arrX.length; c++) {
                //        arrTime.push($.getDate("yyyy-MM-dd", arrX[c]));
                //    }
                //}
                break;
            case "month":
                //if (arrX.length > 0) {
                //    for (var d = 0; d < arrX.length; d++) {
                //        arrTime.push($.getDate("yyyy-MM-dd", arrX[d]));
                //    }
                //}
                break;
            case "timeInterval":
                //if (arrX.length > 0) {
                //    for (var e = 0; e < arrX.length; e++) {
                //        arrTime.push($.getDate("yyyy-MM-dd HH:mm", arrX[e]));
                //    }
                //}
                break;
        }

        //Y轴数据解析
        var yAxis = [];
        var series = [];
        var arrY = data.YAxisDatas;
        if (arrY.length > 0) {
            for (var j = 0; j < arrY.length; j++) {

                var yAxisIndex = j;

                //组装yAxis
                var position = "left";
                if (j > 0) {
                    position = "right";
                }
                var unit = arrY[j].Y.Unit; //单位
                var yAxisObj =
                {
                    position: position,
                    name: unit,
                    nameTextStyle: { color: '#A6A6A6' },//y轴上方单位的颜色
                    type: 'value',
                    axisLabel: { textStyle: { color: "#A6A6A6", fontSize: '13' } },
                    splitLine: { lineStyle: { type: "dashed", color: "#444", show: false } }
                };
                yAxis.push(yAxisObj);

                //组装series
                var seriess = arrY[j].Seriess;
                if (seriess.length > 0) {
                    for (var k = 0; k < seriess.length; k++) {
                        var name = seriess[k].Name;   //名称
                        var datas = seriess[k].Datas; //数据
                        var seriesObj = {
                            data: datas,
                            markPoint: {
                                data: [{ type: 'max', name: 'Max' }, { type: 'min', name: 'Min' }],
                                label: { textStyle: { color: "#FFFFFF", fontSize: 12 } }
                            },
                            name: name,
                            type: 'line',
                            yAxisIndex: yAxisIndex,
                            smooth: true, //开启平滑度
                            symbolSize: 5, //圆点大小
                            zlevel: 3,
                            lineStyle: { //线条粗细
                                width: 2
                            }
                        };
                        series.push(seriesObj);
                    }
                }
            }
        }

        //显示图表
        var dcChart = echarts.init(document.getElementById("dcChart"));
        dcChart.clear(); //重绘之前,先清除一下（否则没有动画效果和清除图表操作）
        if (arrX.length > 0 && yAxis.length > 0 && series.length > 0) {
            $(".data-center-main .empty-data-img").addClass("hide"); //隐藏暂无数据
            var option = {
                tooltip: {
                    trigger: 'axis'
                },
                legend: {
                    x: 'center',
                    data: legData,
                    icon: "circle",
                    itemWidth: 6,  //图标宽
                    itemHeight: 6, //图标高
                    itemGap: 10,   //间距
                    textStyle: {
                        color: '#A6A6A6',
                        fontSize: '13'
                    }
                },
                grid: {
                    top: '50',
                    left: '40',
                    right: '40',
                    bottom: '80',
                    containLabel: true
                },
                toolbox: {
                    show: true,
                    feature: {
                        dataView: {
                            show: true,
                            //readOnly: false,
                            title: '数据视图',
                            lang: ['数据视图', '关闭', '导出Excel'],
                            buttonColor  : '#c23531',
                            optionToContent: function (opt) {
                                var axisData = opt.xAxis[0].data;//x轴作为条件，y轴需改成yAxis[0].data;
                                var series = opt.series;
                                var tdHeads = '<td  style="padding:0 10px">时间</td>';
                                series.forEach(function (item) {
                                    tdHeads += '<td style="padding: 0 10px">' + item.name + '</td>';
                                });
                                var html = '<table border="1" id="myTable" style="margin-left:20px;border-collapse:collapse;font-size:14px;text-align:center;" class="table-striped"><tbody><tr>' + tdHeads + '</tr>';
                                var tdBodys = '';
                                for (var i = 0, l = axisData.length; i < l; i++) {
                                    for (var j = 0; j < series.length; j++) {
                                        if (typeof (series[j].data[i]) == 'object' && series[j].data[i] !== null) {
                                            tdBodys += '<td>' + series[j].data[i].value + '</td>';
                                        } else {
                                            tdBodys += '<td>' + series[j].data[i] + '</td>';
                                        }
                                    }
                                    html += '<tr><td style="padding: 0 10px">' + axisData[i] + '</td>' + tdBodys + '</tr>';
                                    tdBodys = '';
                                }
                                html += '</tbody></table>';
                               /* html += '<button id="exportButton">Export to Excel</button>';*/
                                return html;
                            },
                            contentToOption: function (opts) {

                                $("#myTable").table2excel({

                                    exclude: ".noExl", //过滤位置的 css 类名， 有class = “noExl” 的行不被导出

                                    filename: "dataView", // 文件名称

                                    name: "Excel Document Name.xls",

                                    exclude_img: true,

                                    exclude_links: true,

                                    exclude_inputs: true

                                });

                            },
                        },
                        magicType: { type: ['line', 'bar'] }
                    }
                },
                dataZoom: [
                    {
                        type: 'inside',
                        start: 0,
                        end: 100
                    },
                    {
                        start: 0,
                        end: 100,
                        bottom: '30'
                    }
                ],
                xAxis: {
                    data: arrX,
                    type: 'category',
                    boundaryGap: false,
                    axisLabel: { //坐标轴上的文字
                        textStyle: {
                            color: "#A6A6A6",
                            fontSize: '13'
                        }
                    },
                    axisTick: { //坐标轴刻度线
                        show: false
                    },
                    axisLine: { //坐标轴线
                        lineStyle: {
                            color: "#555"
                        }
                    }
                },
                yAxis: yAxis,
                series: series
            };
            dcChart.setOption(option);
        } else {
            $(".data-center-main .empty-data-img").removeClass("hide"); //显示暂无数据
        }
    };

    

    //获取数据图表数据
    var getDcChartData = function () {
        var sTime = "";
        var eTime = "";
        var paramIds = searchDc.paramIds; //参数ID

        var dataType = ""; //数据类型
        var timeType = searchDc.timeType; //时间类型
        switch (timeType) {
            case "hour": //时
                var timeH = $.getDate("yyyy-MM-dd HH");
                sTime = timeH + ":00:00";
                eTime = timeH + ":59:59";
                dataType = "1";
                break;
            case "day": //日
                var timeD = $.getDate("yyyy-MM-dd");
                sTime = timeD + " 00:00:00";
                eTime = timeD + " 23:59:59";
                dataType = "2";
                break;
            case "week": //周
                sTime = $.getWeekDate("s", 0) + " 00:00:00";
                eTime = $.getWeekDate("e", 0) + " 23:59:59";
                dataType = "3";
                break;
            case "month": //月
                sTime = $.getMonthDate("s", 0) + " 00:00:00";
                eTime = $.getMonthDate("e", 0) + " 23:59:59";
                dataType = "3";
                break;
            case "timeInterval": //时间区间
                sTime = searchDc.sTime;
                eTime = searchDc.eTime;
                dataType = "0";
                break;
        }

        if (!$.isEmpty(paramIds)) {
            $.reqAjax({
                url: "/api/QueryData/GetParameterDatas?dataType=" + dataType + "&stime=" + sTime + "&etime=" + eTime + "&ids=" + paramIds + "",
                type: "get",
                headers: { "token": true },
                success: function (res) {
                    var data = res.Data;
                    renderDcChart(timeType, data); //渲染数据图表
                }
            })
        }
    };
    var a = "";

    //渲染日历
    var renderDcCalendar = function () {
        var calendar = $(".data-center-main #dcCalendar").fullCalendar({
            header: {					    //设置日历头部信息，false，则不显示头部信息。包括left，center,right左中右三个位置
                left: '',					//上一个、下一个、今天
                center: 'title',			//标题
                right: 'prev,next'	//月、周、日、日程列表
            },
            locale: 'zh-cn',		//貌似没用
            timeFormat: 'HH:mm',	//日程事件的时间格式
            buttonText: {    	    //各按钮的显示文本信息
                //today: '今天',
                //month: '月',
                //agendaWeek: '周',
                //agendaDay: '日',
                //listMonth: '日程',
            },
            monthNames: ["1月", "2月", "3月", "4月", "5月", "6月", "7月", "8月", "9月", "10月", "11月", "12月"],      //月份全称
            monthNamesShort: ["1月", "2月", "3月", "4月", "5月", "6月", "7月", "8月", "9月", "10月", "11月", "12月"], //月份简写
            dayNames: ["周日", "周一", "周二", "周三", "周四", "周五", "周六"],    	  //周全称
            dayNamesShort: ["周日", "周一", "周二", "周三", "周四", "周五", "周六"],  //周简写
            noEventsMessage: "当月无数据",   							              //listview视图下，无数据提示
            allDayText: "全天",											              //自定义全天视图的名称
            allDaySlot: false,  										              //是否在周日历上方显示全天
            slotDuration: "00:30:00",      							                  //一格时间槽代表多长时间，默认00:30:00（30分钟）
            slotLabelFormat: "H(:mm)a",    							                  //日期视图左边那一列显示的每一格日期时间格式
            slotLabelInterval: "01:00:00", 							    //日期视图左边那一列多长间隔显示一条日期文字(默认跟着slotDuration走的，可自定义)
            snapDuration: "01:00:00",      							    //其实就是动态创建一个日程时，默认创建多长的时间块
            firstDay: 1,   												//一周中显示的第一天是哪天，周日是0，周一是1，类推
            hiddenDays: [],												//隐藏一周中的某一天或某几天，数组形式，如隐藏周二和周五：[2,5]，默认不隐藏，除非weekends设置为false。
            weekends: true,												//是否显示周六和周日，设为false则不显示周六和周日。默认值为true
            weekMode: 'fixed',											//月视图里显示周的模式，因每月周数不同月视图高度不定。fixed：固定显示6周高，日历高度保持不变liquid：不固定周数，高度随周数变化variable：不固定周数，但高度固定
            weekNumbers: false,											//是否在日历中显示周次(一年中的第几周)，如果设置为true，则会在月视图的左侧、周视图和日视图的左上角显示周数。
            weekNumberCalculation: 'iso',								//周次的显示格式。
            height: 'auto',												//设置日历的高度，包括header日历头部，默认未设置，高度根据aspectRatio值自适应。
            handleWindowResize: true,									//是否随浏览器窗口大小变化而自动变化。
            defaultView: 'month',										//初始化时默认视图，默认是月month，agendaWeek是周，agendaDay是当天
            //defaultDate: '2022-09-01',						        //默认显示那一天的日期视图getDates(true)2020-05-10
            nowIndicator: true,            								//周/日视图中显示今天当前时间点（以红线标记），默认false不显示
            eventLimit: false,       									//数据条数太多时，限制显示条数（多余的以“+2more”格式显示），默认false不限制,支持输入数字设定固定的显示条数
            eventLimitText: "更多",       								//当一块区域内容太多以"+2 more"格式显示时，这个more的名称自定义（应该与eventLimit: true一并用）
            dayPopoverFormat: "YYYY年M月d日", 							//点开"+2 more"弹出的小窗口标题，与eventLimitClick可以结合用
            render: function (view) {								    //method,绑定日历到id上。$('#id').fullCalendar('render');
            },
            events: function (start, end, timezone, callback) {
            },
            viewRender: function () {
            },
            dayClick: function (date) { //空白的日期区点击
                //alert('Clicked on: ' + date.format());
                var date1 = new Date(date.format());
                var dayOfWeek = date1.getDay();
                var day = "";
                if (dayOfWeek === 0) { // 星期日
                    day ="星期日"
                } else if (dayOfWeek === 6) { // 星期六
                    day = "星期六"
                } else if (dayOfWeek === 5) { // 星期五
                    day = "星期五"
                } else if (dayOfWeek === 4) { // 星期四
                    day = "星期四"
                } else if (dayOfWeek === 3) { // 星期三
                    day = "星期三"
                } else if (dayOfWeek === 2) { // 星期二
                    day = "星期二"
                } else if (dayOfWeek === 1) { // 星期一
                    day = "星期一"
                }
                var paramIds = searchDc.paramIds; //参数ID
                $.layer.open({
                    title: a + "&nbsp;&nbsp;&nbsp;" + day + "&nbsp;&nbsp;&nbsp;" + date.format(), content: "Date_Description.html?paramDate=" + date.format() +"&paramIds=" + paramIds + "", width: "1000px", height: "600px" });
                
            },
            eventClick: function (data) {
            },
            eventMouseover: function () { },					//鼠标划过和离开的事件，用法和参数同上
            eventMouseout: function () { },
            selectable: true,									//是否允许通过单击或拖动选择日历中的对象，包括天和时间
            selectHelper: true,   								//当点击或拖动选择时间时，是否预先画出“日程区块”的样式显示默认加载的提示信息，该属性只在周/天视图里可用
            selectMirror: true,									//镜像
            selectOverlap: false,       						//是否允许选择被事件占用的时间段，默认true可占用时间段
            selectAllow: function (selectInfo) { 				//精确的控制可以选择的地方，返回true则表示可选择，false表示不可选择             
            },
            select: function (date) { //点击空白区域/选择区域内容触发
            },
            unselect: function (view, jsEvent) { //选择取消时触发   
            },
            lazyFetching: true,        							//是否启用懒加载技术--即只取当前条件下的视图数据，其它数据在切换时触发，默认true只取当前视图的，false是取全视图的
            defaultTimedEventDuration: "02:00:00",     			//在Event Object中如果没有end参数时使用，如start=7:00pm，则该日程对象时间范围就是7:00~9:00
            defaultAllDayEventDuration: { days: 1 },  			//默认1天是多长，（有的是采用工作时间模式，所以支持自定义）
            editable: true,                 					//支持日程拖动修改，默认false
            dragOpacity: 0.2,                					//拖拽时不透明度，0.0~1.0之间，数字越小越透明
            dragScroll: true,              						//是否在拖拽时自动移动容器，默认true
            eventOverlap: true,            						//拖拽时是否重叠
            eventConstraint: {     								//限制拖拽拖放的位置（即限制有些地方拖不进去）t
                dow: [1, 2, 3, 4, 5, 6, 0] 						//0是周日，1是周一，以此类推
            },
            longPressDelay: 1000,  								//移动设备，长按多少毫秒即可拖动,默认1000毫秒（1S）
            eventDragStart: function (event, jsEvent, ui, view) {    //日程开始拖拽时触发
            },
            eventDragStop: function (event, jsEvent, ui, view) {     //日程拖拽停止时触发
            },
            eventDrop: function (event, dayDelta, delta, revertFunc, jsEvent, ui, view) {  //日程拖拽停止，并且时间改变时触发，ayDelta日程前后移动了多少天
            },
            eventResizeStart: function (event, jsEvent, ui, view) { //日程大小调整开始时触发
            },
            eventResizeStop: function (event, jsEvent, ui, view) {     //日程大小调整停止时触发
            },
            eventResize: function (event, delta, revertFunc, jsEvent, ui, view) { //日程大小调整完成并已经执行更新时触发
            },
        });
    }

    //渲染日历图表
    var renderDcCalendarChart = function (data) {
        debugger;
        var index = 0;
        $(".data-center-main #dcCalendar .fc-day-grid .fc-row").each(function () {
            $(this).find(".fc-bg table tbody tr td").each(function () {
                index++;
                var date = $(this).attr("data-date"); //日历日期
                var d = new Date(date);
                var tdW = $(this).width(); //列宽度
                //<div class=\"shuziDiv\" style=\"background:url(../images/shuzi/shuzi" + index + ".png); width:100%;height:100%;\">
                $(this).html(" <div id=\"graph" + index + "\" class=\"graph\" style=\"width:" + tdW + "px; background:url(../images/shuzi/shuzi" + d.getDate() +".png) no-repeat center; min-height : 200px;\"></div>"); //生成图表元素

             
                var dcChart = echarts.init(document.getElementById("graph" + index + ""));

                //获取当前日期的数据
                var res = data.filter((item, index) => {
                    var otherData = item.OtherData;
                    if (otherData.indexOf(date) !== -1) {
                        return item;
                    }
                });
                var fiterNumer = 5;
                if (res.length > 0) {

                    //X轴数据解析
                    //var arrX = res[0].XAxisData.filter(function (item, index) {

                    //    if (index % fiterNumer === 0||index==0) {
                    //        return item;
                    //    }

                    //});
                    var arrX = res[0].XAxisData;
                    //var arrTime = []; //时间
                    //if (arrX.length > 0) {
                    //    for (var a = 0; a < arrX.length; a++) {
                    //        arrTime.push($.getDate("HH:mm", arrX[a]));
                    //    }
                    //}

                    //Y轴数据解析
                    var yAxis = [];
                    var series = [];
                    //var arrY = res[0].YAxisDatas.filter(function (item, index) {

                    //    if (index % fiterNumer === 0 || index == 0) {
                    //        return item;
                    //    }

                    //});
                    var arrY = res[0].YAxisDatas;
                   
                  
                    if (arrY.length > 0) {                    
                        for (var j = 0; j < arrY.length; j++) {                        
                            var yAxisIndex = j;

                            //组装yAxis
                            var position = "left";
                            if (j > 0) {
                                position = "right";
                            }
                            var unit = arrY[j].Y.Unit; //单位
                            var yAxisObj =
                            {
                                position: position,
                                name: unit,
                                nameTextStyle: { color: '#666' },//y轴上方单位的颜色
                                type: 'value',
                                axisLabel: {
                                    show: false,
                                    textStyle: { color: "#A6A6A6", fontSize: '12' }
                                },
                                splitLine: {
                                    lineStyle: { type: "dashed", color: "#E6E6E6" }
                                }
                            };
                            yAxis.push(yAxisObj);
                           
                            //组装series
                            var seriess = arrY[j].Seriess;
                            if (seriess.length > 0) {                               
                                for (var k = 0; k < seriess.length; k++) {
                                    var name = seriess[k].Name;   //名称
                                    var datas = seriess[k].Datas; //数据
                                    //var showData = datas.filter(function (item, index) {
                                    //    if (index % fiterNumer === 0 || index == 0) {
                                    //     return item;
                                    //        }

                                    //    });
                                    var seriesObj = {
                                        data: datas,
                                        name: name,
                                        type: 'line',
                                        yAxisIndex: yAxisIndex,
                                        smooth: true, //开启平滑度
                                        symbolSize: 5, //圆点大小
                                        showSymbol: false,
                                        zlevel: 3,
                                        lineStyle: { //线条粗细
                                            width: 2
                                        }
                                    };                                                              
                                    series.push(seriesObj);
                                }
                            }                          
                        }
                    }
                    //显示图表
                    if (arrX.length > 0 && yAxis.length > 0 && series.length > 0) {
                        var option = {
                            tooltip: {
                                trigger: 'axis'
                            },
                            grid: {
                                top: '35',
                                left: '40',
                                right: '15',
                                bottom: '10',
                                containLabel: true
                            },
                            xAxis: {
                                data: arrX,
                                type: 'category',
                                boundaryGap: false,
                                axisLabel: { //坐标轴上的文字
                                    textStyle: {
                                        color: "#666",
                                        fontSize: '12'
                                    }
                                },
                                axisTick: { //坐标轴刻度线
                                    show: false
                                },
                                axisLine: { //坐标轴线
                                    lineStyle: {
                                        color: "#E9E9E9"
                                    }
                                },
                            },
                            yAxis: yAxis,
                            series: series
                        };

                        dcChart.clear(); //重绘之前,先清除一下（否则没有动画效果和清除图表操作）
                        dcChart.setOption(option);
                    }
                }
            });
        });
    }

    var initVideo = function (data) {
        console.log(data)
        var sysItemId = ""; //系统项ID(iframe调用外部index页面元素)

        let jsonConfig=JSON.parse(data.JsonConfig)
        
        /*let jsonConfig = { //Json配置数据
            pageW: pageW,
            pageH: pageH,
            appKey: appKey,
            appSecret: appSecret,
            deviceSerial: deviceSerial,
            deviceCode: deviceCode
        };*/

 
        };*/
        //给磁贴数据元素添加属性与样式
        let id = data.Id;
        let x = data.X;
        let y = data.Y;
        let pageW = jsonConfig.pageW;
        let pageH = jsonConfig.pageH;
        let fontSize = data.FontSize;
        let textColor = data.TextColor;
        let bgColor = data.BackgroundColor;
        debugger
         //       $(elem)
           //         .attr({ "elem-id": data.Id, "elem-left": data.x, "elem-top": data.y, "elem-pageW": jsonConfig.pageW, "elem-pageH": jsonConfig.pageH })
             
       
        var html = "";
        html += "<div metro-type=\"videoText\" class=\"item video-text ui-draggable ui-draggable-handle ui-draggable-dragging\" elem-index=\"videoText-" + id + "\" elem-id=\"" + id + "\" elem-left=\"" + x + "\" elem-top=\"" + y + "\" elem-pageW=\"" + pageW + "\" elem-pageH=\"" + pageH + "\" style=\"position: absolute; z-index: 9; left:" + x + "px; top:" + y + "px;font-size:" + fontSize + "px;color:" + textColor + ";background:" + bgColor + " \">";
        html += "<div class=\"metro-card-title\"></div>";
        html += "<div class=\"metro-card-data\">";
        html += "</div>";

        let videoKey = 'playWind' + data.Id;

        let obj = {
            id:data.Id,
            videoKey: videoKey,
            width: jsonConfig.pageW - 30,
            height: jsonConfig.pageH - 50,
            object: html,
         }
        return obj;
    }

    //获取日历数据
    var getDcCalendarData = function () {
        var paramIds = searchDc.paramIds; //参数ID

        //检查是否拖拽多个参数ID
        if (paramIds.indexOf(",") !== -1) {
            paramIds = paramIds.split(",")[0]; //截取第一个逗号的参数ID
        }

        var year = searchDc.year;   //年份
        var month = searchDc.month; //月份

      
       var layerInfo= layer.msg("加载中", {
           icon: 16
           , shade: 0.3
           , time: false
        })
        $.reqAjax({
            url: "/api/QueryData/GetCalendarData?year=" + year + "&mon=" + month + "&id=" + paramIds + "",
          
            type: "get",
            headers: { "token": true },
            success: function (res) {
                var data = res.Data;
                a = data[0].LegendData[0];
                layer.close(layerInfo);
                if (!$.isEmpty(data)) {
                    renderDcCalendarChart(data);
                }
            }
        })
    }

   
   
    //上一个月事件
    $(".data-center-main #dcCalendar").on("click", ".fc-prev-button", function () {
        var date = $('#dcCalendar').fullCalendar('getDate').format('YYYY-MM'); //获得操作后的日期
        var arrDate = date.split("-");
        searchDc.year = arrDate[0];  //年份
        searchDc.month = arrDate[1]; //月份

        renderDcCalendar();  //渲染日历
        getDcCalendarData(); //获取日历数据
    });

    //下一个月事件
    $(".data-center-main #dcCalendar").on("click", ".fc-next-button", function () {
        var date = $('#dcCalendar').fullCalendar('getDate').format('YYYY-MM'); //获得操作后的日期
        var arrDate = date.split("-");
        searchDc.year = arrDate[0];  //年份
        searchDc.month = arrDate[1]; //月份

        renderDcCalendar();  //渲染日历
        getDcCalendarData(); //获取日历数据
    });

    //基准设置
    $(".baseline-btn").on("click", function () {
        var paramId = $(".data-center-main .metro-tree").jstree("get_selected"); //获取选中参数ID

        $.layer.open({ title: "基准设置", content: "rule/list.html?paramId=" + paramId + "", width: "600px", height: "600px" });
    });

    //////////////////////////////////////////消息通知/////////////////////////////////////////

    //消息通知点击事件
    $(".navbar-nav .notice").on("click", function () {
        $('.notice').css("animation-play-state", 'paused');
        document.getElementById("notice-sidebar").style.width = "495px";
        document.getElementById("metro-main").style.width = "0px";
        document.getElementById("parameter-sidebar").style.width = "0px";
        getAlarmData(); //获取报警数据
    });

    //获取报警数据
    var getAlarmData = function () {
        $("#notice-sidebar #dataAlarm table tbody").html(""); //清空数据报警
        $("#notice-sidebar #signAlarm table tbody").html(""); //清空通讯报警

     
        $.reqAjax({
            url: "/api/System/InitAlarmData",
            type: "get",
            headers: { "token": true },
            before: function () {
                $(".spinner-border").removeClass("hide");
            },
            success: function (res) {
                var data = res.Data.Data;
          
                if (data.length > 0) {
                 
                    //获取数据报警
                    var dataAlarm = data.filter((item, index) => {
                        if (item.AlarmEnum === 0) {
                            return item;
                        }
                    });
                    if (dataAlarm.length > 0) {
                        var dataHtml = "";
                        for (var i = 0; i < dataAlarm.length; i++) {

                            //var bgColor = "";
                            var dataAlarmLevel = dataAlarm[i].AlarmLevel; //报警级别
                            switch (dataAlarmLevel) {
                                case 0:
                                    //bgColor = "alert-secondary";
                                    //dataAlarmLevel = "无";
                                    dataAlarmLevel = "<span class=\"badge badge-light\">无</span>";
                                    break;
                                case 1:
                                    //bgColor = "alert-info";
                                    //dataAlarmLevel = "普通";
                                    dataAlarmLevel = "<span class=\"badge badge-info\">普通</span>";
                                    break;
                                case 2:
                                    //bgColor = "alert-warning";
                                    //dataAlarmLevel = "重要";
                                    dataAlarmLevel = "<span class=\"badge badge-warning\">重要</span>";
                                    isShark = true;
                                    break;
                                case 3:
                                    //bgColor = "alert-danger";
                                    //dataAlarmLevel = "错误";
                                    dataAlarmLevel = "<span class=\"badge badge-danger\">错误</span>";
                                    isShark = true;
                                    break;
                                default:
                                    dataAlarmLevel = "";
                                    break;
                            }

                            var id = dataAlarm[i]._id;                    //报警记录ID
                            var isRead = dataAlarm[i].IsRead;             //是否阅读

                            dataHtml += "<tr id=" + id + " isRead=" + isRead + ">";
                            dataHtml += "<td>" + dataAlarm[i].Msg + "</td>";
                            dataHtml += "<td>" + dataAlarmLevel + "</td>";
                            dataHtml += "<td>" + dataAlarm[i].Time + "</td>";
                            dataHtml += "<td>" + (dataAlarm[i].IsRead === true ? "<span class=\"text-disable\">已阅</span>" : "<span class=\"text-danger\">未阅</span>") + "</td>";
                            dataHtml += "</tr>";
                        }
                        $("#notice-sidebar #dataAlarm table tbody").html(dataHtml);
                    }

                    //获取通讯报警
                    var signAlarm = data.filter((item, index) => {
                        if (item.AlarmEnum === 1) {
                            return item;
                        }
                    });
                    if (signAlarm.length > 0) {
                        var signHtml = "";
                        for (var j = 0; j < signAlarm.length; j++) {

                            //var bgColor = "";
                            var signAlarmLevel = signAlarm[j].AlarmLevel; //报警级别
                            switch (signAlarmLevel) {
                                case 0:
                                    //bgColor = "alert-secondary";
                                    //signAlarmLevel = "无";
                                    signAlarmLevel = "<span class=\"badge badge-light\">无</span>";
                                    break;
                                case 1:
                                    //bgColor = "alert-info";
                                    //signAlarmLevel = "普通";
                                    signAlarmLevel = "<span class=\"badge badge-info\">普通</span>";
                                    break;
                                case 2:
                                    //bgColor = "alert-warning";
                                    //signAlarmLevel = "重要";
                                    signAlarmLevel = "<span class=\"badge badge-warning\">重要</span>";
                                    break;
                                case 3:
                                    //bgColor = "alert-danger";
                                    //signAlarmLevel = "错误";
                                    signAlarmLevel = "<span class=\"badge badge-danger\">错误</span>";
                                    break;
                                default:
                                    signAlarmLevel = "";
                                    break;
                            }

                            signHtml += "<tr>";
                            signHtml += "<td>" + signAlarm[j].Msg + "</td>";
                            signHtml += "<td>" + signAlarmLevel + "</td>";
                            signHtml += "<td>" + signAlarm[j].Time + "</td>";
                            signHtml += "</tr>";
                        }
                        $("#notice-sidebar #signAlarm table tbody").html(signHtml);
                    }
                }
            },
            complete: function () {
                $(".spinner-border").addClass("hide");
            },
            time: 500,
            spinner: true
        })
    };

    //表格行点击事件
    $("#notice-sidebar #dataAlarm table tbody").on("click", "tr", function () {
        var obj = $(this);
        var id = $(this).attr("id");         //报警记录ID
        var isRead = $(this).attr("isRead"); //是否阅读
        if (isRead === "false" && !$.isEmpty(id)) {
            //Ajax请求
            $.reqAjax({
                url: "/api/System/AlarmRead/" + id + "",
                type: "get",
                headers: { "token": true },
                success: function (res) {
                    obj.children("td").eq(3).children("span").html("已阅").attr("class", "text-disable");
                }
            })
        }
        $.layer.open({ title: "报警详情", content: "rule/details.html?alarmId=" + id + "", width: "450px", height: "500px" });
    });


    //////////////////////////////////////////报表管理/////////////////////////////////////////

    //打开页面
    $(".navbar-nav .report-manage").on("click", function () {
        $.layer.open({ title: "报表管理", content: "report/list.html", width: "500px", height: "500px" });
    });


    //////////////////////////////////////////我的账户/////////////////////////////////////////
    //关闭个人中心下拉层
    const $menu = $(".account-profile")
    $(document).mouseup(e => {
        if (!$menu.is(e.target) // if the target of the click isn't the container...
            &&
            $menu.has(e.target).length === 0) // ... nor a descendant of the container
        {
            $menu.removeClass('account-link-show');
        }
    });


    //获取用户信息
    var userInfo = {};
    var getUserInfo = function () {
        $.reqAjax({
            url: "/api/Sys_User/GetUserInfo",
            type: "get",
            headers: { "token": true },
            success: function (res) {
                userInfo = res.Data.userinfo; //用户信息
                let selectedCompany = res.Data.selectedCompany; //选择企业

                //个人头像 
                let className = "";
                let headUrl = userInfo.HeadUrl;
                if (!$.isEmpty(headUrl)) {
                    headUrl = api_service + headUrl;
                } else {
                    className = "df-avatar";
                    headUrl = "images/avatar.png";
                }

                //性别
                let gender = userInfo.Gender;
                switch (gender) {
                    case 0:
                        gender = "男";
                        break;
                    case 1:
                        gender = "女";
                        break;
                    default:
                        gender = "未知";
                        break;
                }
                let userName = userInfo.Name; //姓名
                let email = userInfo.EmailId; //邮箱
                let phone = userInfo.Phone;   //电话

                let compName = selectedCompany.SysCompany.Name; //企业名称

                $(".my-account-main .my-account-view .user-avatar").html("<img class=\"" + className + "\" src=\"" + headUrl + "\"/>");
                $(".my-account-main .my-account-view .comp-name").html(compName);
                $(".my-account-main .my-account-view .user-name").text(userName);
                $(".my-account-main .my-account-view .gender").html(gender);
                $(".my-account-main .my-account-view .email").html(email);
                $(".my-account-main .my-account-view .phone").html(phone);
            },
            complete: function () {
                $(".my-account-main").addClass("my-account-main-show"); //显示我的账户层
            }
        })
    };

    //个人中心
    $(".navbar-nav .account-profile").click(function () {
        $(".account-profile").toggleClass("account-link-show");
    });

    //我的账户
    $(".my-account-link").click(function () {
        getUserInfo();
    });
    $(".ai-link").click(function () {
        $.layer.open({ title: " ", content: "chatAI.html", width: "600px", height: "650px" });
    })
    $(".log-link").click(function () {
        $.layer.open({ title: "版本日志列表 ", content: "log_link.html", width: "1100px", height: "670px" });
    })
    //关闭我的账户层
    $(".my-account-close-icon").click(function () {
        $(".my-account-main").removeClass("my-account-main-show");
    });

    //编辑
    $(".my-account-main .btn-account-edit").click(function () {
        if (!$.isEmpty(userInfo)) {
            var className = "";
            var headUrl = userInfo.HeadUrl; //个人头像 
            if (!$.isEmpty(headUrl)) {
                headUrl = api_service + headUrl;
            } else {
                className = "df-avatar";
                headUrl = "images/add-avatar.png";
            }
            var userName = userInfo.Name;   //姓名
            var gender = userInfo.Gender;   //性别
            var email = userInfo.EmailId;   //邮箱
            var phone = userInfo.Phone;     //电话
            var remark = userInfo.Remark;   //备注

            $(".my-account-main .my-account-edit .user-avatar").html("<img class=\"" + className + "\" src=\"" + headUrl + "\"/>");
            $(".my-account-main .my-account-edit #hiddAvatar").val(userInfo.HeadUrl).attr("data-img-base64", userInfo.HeadUrl).change();
            $(".my-account-main .my-account-edit #txtUserName").val(userName);
            $(".my-account-main .my-account-edit #sltGender").val(gender);
            $(".my-account-main .my-account-edit #txtEmail").val(email);
            $(".my-account-main .my-account-edit #txtPhone").val(phone);
            $(".my-account-main .my-account-edit #txtrRemark").val(remark);
        }

        $(this).hide(); //隐藏编辑按钮
        $(".my-account-main .my-account-view").hide(); //隐藏账户预览
        $(".my-account-main .my-account-edit").show(); //隐藏账户编辑
    });

    //上传头像
    $(".my-account-main .my-account-edit #uploadAvatar").on("change", function (e) {
        var file = e.target.files[0];
        if (!$.isEmpty(file)) {
            var fileName = file.name; //文件名
            if (!$.isEmpty(fileName)) {
                var reader = new FileReader(); //创建文件读取对象
                reader.readAsDataURL(file);    //将读取到的文件编码成base64url
                reader.onload = function () {
                    var result = reader.result;
                    $(".my-account-main .my-account-edit #hiddAvatar").val(fileName).attr("data-img-base64", result).change(); //赋值hidden

                    //检查文件格式
                    var className = "";
                    var headUrl = "";
                    if (/\.(png|jpeg|jpg|gif)$/i.test(fileName)) {
                        headUrl = result;
                    } else {
                        headUrl = "images/add-avatar.png";
                        className = "df-avatar";
                    }

                    //显示上传头像
                    $(".my-account-main .my-account-edit .user-avatar").html("<img class=\"" + className + "\" src=\"" + headUrl + "\" />");
                };
            }
        }
    });

    //开启表单验证
    $(".my-account-main #accountForm").bootstrapValidator({
        excluded: [":disabled"],
        fields: {
            hiddAvatar: {
                trigger: "change", //关键配置
                validators: {
                    //notEmpty: {
                    //    message: "请上传图片"
                    //},
                    regexp: {
                        regexp: /\.(png|PNG|jpeg|JPEG|jpg|JPG|gif|GIF)$/,
                        message: "上传格式不正确"
                    }
                }
            }
        }
    });

    //保存
    $(".my-account-main #btnSaveAccount").click(function () {
        var isValid = $(".my-account-main #accountForm").data("bootstrapValidator").validate().isValid();
        if (isValid) {

            //表单数据
            var compId = userInfo.CompanyId; //公司ID
            var userId = userInfo.Id;        //用户ID
            var userName = $(".my-account-main .my-account-edit #txtUserName").val(); //用户名
            var headUrl = $(".my-account-main .my-account-edit #hiddAvatar").attr("data-img-base64"); //用户头像
            var gender = $(".my-account-main .my-account-edit #sltGender").val();  //性别
            var email = $(".my-account-main .my-account-edit #txtEmail").val();    //邮箱
            var phone = $(".my-account-main .my-account-edit #txtPhone").val();    //电话
            var remark = $(".my-account-main .my-account-edit #txtrRemark").val(); //备注

            //请求数据
            var reqData = {
                CompanyId: compId,
                Id: userId,
                Name: userName,
                HeadUrl: headUrl,
                Gender: gender,
                EmailId: email,
                Phone: phone,
                Remark: remark
            };

            //Ajax请求
            $.reqAjax({
                url: "/api/Sys_User/Update",
                data: reqData,
                headers: { "token": true },
                success: function (res) {
                    $.layer.msg({ info: "保存成功", icon: "success", shift: 0 });

                    $(".my-account-main .my-account-edit").hide();  //隐藏账户编辑
                    $(".my-account-main .my-account-view").show();  //显示账户预览
                    $(".my-account-main .btn-account-edit").show(); //显示编辑按钮

                    getUserInfo(); //获取用户信息
                },
                load: true, elem: $(this)
            });
        }
    });

    

    //返回
    $(".my-account-main #btnReturnAccount").on("click", function () {
        $(".my-account-main .my-account-edit").hide();  //隐藏账户编辑
        $(".my-account-main .my-account-view").show();  //显示账户预览
        $(".my-account-main .btn-account-edit").show(); //显示编辑按钮
    });


    //////////////////////////////////////////修改密码/////////////////////////////////////////
    //修改密码
    $(".password-link").click(function () {
        $(".password-main").addClass("password-main-show"); //显示修改密码层
    });

    //关闭修改密码层
    $(".password-close-icon").click(function () {
        $(".password-main").removeClass("password-main-show");
    });

    //开启表单验证
    $(".password-main #passwordForm").bootstrapValidator();

    //保存
    $(".password-main #btnSavePassword").click(function () {
        var isValid = $(".password-main #passwordForm").data("bootstrapValidator").validate().isValid();
        if (isValid) {

            //表单数据
            var oldPassword = $(".password-main #txtOldPassword").val(); //原密码
            var confirmNewPassword = $(".password-main #txtConfirmNewPassword").val(); //确认新密码

            //Ajax请求
            $.reqAjax({
                url: "/api/Sys_User/UpdatePwd?oldpwd=" + oldPassword + "&newpwd=" + confirmNewPassword + "",
                headers: { "token": true },
                success: function (res) {
                    if (res.Data) {
                        $.layer.msg({ info: "保存成功", icon: "success", shift: 0 });
                        document.querySelector(".password-main #passwordForm").reset(); //清空表单
                    } else {
                        $.layer.msg({ info: "原密码不正确", icon: "warn" });
                    }
                },
                load: true, elem: $(this)
            });
        }
    });

    //////////////////////////////////////////注销登录/////////////////////////////////////////
    //注销登录
    $(".logout-link").click(function () {
        $.layer.confirm({
            content: "您确定要注销登录吗？", icon: "quest", ok: function () {
                $.reqAjax({
                    url: "api/Login/Logout",
                    headers: { "token": true },
                    success: function (res) {
                        window.localStorage.clear(); //清空浏览器存储数据
                        window.location.href = "../login.html";
                    }
                });
            }
        });
    });

    //////////////////////////////////////////功能磁贴/////////////////////////////////////////
    //能耗
    $(".metro-grid .electric-energy").on("click", function () {
        document.getElementById("parameter-sidebar").style.width = "495px";
        getMetroParamTree(); //获取磁铁参数树形
    });

    //新闻
    $(".metro-grid .information-news").on("click", function () {

    });

    //获取磁铁参数树形
    var getMetroParamTree = function () {
        $.reqAjax({
            url: "api/Sys_Area/GetTree",
            type: "get",
            headers: { "token": true },
            success: function (res) {
                var data = res.Data;
                if (data.length > 0) {
                    for (var i = 0; i < data.length; i++) {
                        var parent = data[i].parent === "#" ? 0 : data[i].parent; //父级ID
                        var type = data[i].type; //参数类型
                        switch (type) {
                            case -1: //区域
                                data[i].icon = "../images/jstree/p1.png";
                                break;
                            case 0: //参数
                                data[i].icon = "../images/jstree/p2.png";
                                break;
                            case 1: //价格
                                data[i].icon = "../images/jstree/p4.png";
                                break;
                            case 2: //虚拟参数
                                data[i].icon = "../images/jstree/p3.png";
                                break;
                        }

                        data[i].state = { "opened": true }; //节点展开状态
                        data[i].a_attr = { "data-type": type, "parent-id": parent }; //节点自定义属性值
                    }

                    var $tree = $("#parameter-sidebar .metro-tree"); //树形对象
                    $tree.jstree({ //绑定树形数据
                        "core": {
                            "data": data,
                            "check_callback": true,
                            "multiple": true,
                        }
                    });

                    //树形节点完成加载后回调
                    $tree.on('ready.jstree', function () {
                        initDynElemDrag("tree-param"); //初始化动态元素拖拽（树形参数）
                    });
                }
            },
            complete: function () {

            }
        })
    };


    //////////////////////////////////////////SignalR//////////////////////////////////////////

    //引用自动生成的代理，kibahub是HubName注解属性
    $.connection.hub.url = api_service + "signalr";
    var kibaHub = $.connection.pushDataHub;

    $.connection.hub.stateChanged(function (status_value) {
        //console.log(status_value);
    });
    //动态绑定实时参数ID
    var bindParamIds = function (ids) {
        //当连接状态变成断开后自动不断重新连接
        $.connection.hub.disconnected(function () {
            if ($.connection.hub.lastError) {
                console.log("Disconnected. Reason: " + $.connection.hub.lastError.message + "重新连接 ...");
                setTimeout(function () {
                    $.connection.hub.start().done(function () {
                        console.log("连接成功！");
                        kibaHub.server.bindmfids(ids);
                    }).fail(function (data) {
                        console.log("fail=" + data);
                    });
                }, 5000);
            }
        });

        //开始连接
        $.connection.hub.start().done(function () {
            console.log('Now connected, connection ID=' + $.connection.hub.id);
            //绑定需要实时刷新的参数id数组
            kibaHub.server.bindmfids(ids);
            console.log("Send done");
            //每个10秒发送心跳包给服务器
            setInterval(function () {
                var d = new Date()
                kibaHub.server.heartbeat(d.toLocaleString());
            }, 10000)
        }).fail(function () { console.log('Could not Connect!'); });
    }

    //服务主动推送数据接口
    kibaHub.client.pushdata = function (message) {
        //console.log("实时数据：" + JSON.stringify(message));
        if (message.length > 0) {
            for (var i = 0; i < message.length; i++) {
                var pId = message[i].Id;     //参数ID
                var pValue = message[i].Val; //参数值
                var dataType = message[i].DataType; //数据类型
                //磁铁数据(添加实时)
                $(".carousel-inner .total-active-energy .metro-energy-list ul li[real-param-id='" + pId + dataType + "']").children("span.value").html(pValue);

                //磁铁参数(添加实时)
                //$(".carousel-inner .jstree-node[real-param-id='" + pId + dataType + "'] .jstree-data em.value").html(pValue);
                $(".carousel-inner a.jstree-anchor[real-param-id='" + pId + dataType + "'] .jstree-data em.value").html(pValue);
                //开关(添加实时)
                var sw = $(".carousel-inner .operation-status[real-param-id='" + pId + dataType + "']"); //获取开关元素
                if (sw.length > 0) {
                    var content = $(sw).attr("param-content"); //参数内容
                    if (!$.isEmpty(content)) {
                        var data = eval('(' + content + ')');
                        if (data.length > 0) {
                            var list = data.filter((item, index) => {
                                if (item.Val === pValue) {
                                    return item;
                                }
                            });
                            if (list.length > 0) {
                                var picture = list[0].Picture;
                                $(sw).find(".metro-operation-status-col.off img").attr("src", api_service + picture);
                            }
                        }
                    }
                }

            }
        }
    };

    //服务主动推送数据报警
    kibaHub.client.pushalarm = function (message) {
        if (message.length > 0) {

            SharkFC(message);
            //推送数据报警
            var dataAlarm = message.filter((item, index) => {
                if (item.AlarmEnum === 0) {
                    return item;
                }
            });        
            if (dataAlarm.length > 0) {
                for (var i = 0; i < dataAlarm.length; i++) {

                    //var bgColor = "";
                    var dataAlarmLevel = dataAlarm[i].AlarmLevel; //报警级别
                    switch (dataAlarmLevel) {
                        case 0:
                            //bgColor = "alert-secondary";
                            //dataAlarmLevel = "无";
                            dataAlarmLevel = "<span class=\"badge badge-light\">无</span>";
                            break;
                        case 1:
                            //bgColor = "alert-info";
                            //dataAlarmLevel = "普通";
                            dataAlarmLevel = "<span class=\"badge badge-info\">普通</span>";
                            break;
                        case 2:
                            //bgColor = "alert-warning";
                            //dataAlarmLevel = "重要";
                            dataAlarmLevel = "<span class=\"badge badge-warning\">重要</span>";
                            break;
                        case 3:
                            //bgColor = "alert-danger";
                            //dataAlarmLevel = "错误";
                            dataAlarmLevel = "<span class=\"badge badge-danger\">错误</span>";
                            break;
                        default:
                            dataAlarmLevel = "";
                            break;
                    }

                    var id = dataAlarm[i]._id;                    //报警记录ID
                    var isRead = dataAlarm[i].IsRead;             //是否阅读
                    var alarmMsg = dataAlarm[i].Msg;              //报警信息
                    var alarmLevel = dataAlarm[i].AlarmLevel;     //报警级别
                    var alarmTime = dataAlarm[i].Time;            //报警时间
                    var ruleValue = dataAlarm[i].RuleValue;       //设定值
                    var triggerValue = dataAlarm[i].TriggerValue; //触发值

                    var dataHtml = "";
                    dataHtml += "<tr id=" + id + " isRead=" + isRead + " alarmMsg=" + alarmMsg + " alarmLevel=" + alarmLevel + " alarmTime='" + alarmTime + "' ruleValue=" + ruleValue + " triggerValue=" + triggerValue + " class=\"bg-simple-danger\">";
                    dataHtml += "<td class=\"text-high-danger\">" + dataAlarm[i].Msg + "</td>";
                    dataHtml += "<td>" + dataAlarmLevel + "</td>";
                    dataHtml += "<td class=\"text-high-danger\">" + dataAlarm[i].Time + "</td>";
                    dataHtml += "<td>" + (dataAlarm[i].IsRead === true ? "<span class=\"text-disable\">已阅</span>" : "<span class=\"text-danger\">未阅</span>") + "</td>";
                    dataHtml += "</tr>";

                    var dataTbody = $("#notice-sidebar #dataAlarm table tbody");
                    var dataRows = dataTbody.find("tr").length;
                    if (dataRows <= 0) {
                        dataTbody.append(dataHtml); //表格结尾插入内容
                    } else {
                        dataTbody.find("tr:first").before(dataHtml); //第一行前面插入内容
                    }

                    //检查表格行数
                    if (dataRows >= 50) {
                        dataTbody.find("tr:last").remove(); //移除最后一行
                    }
                }
            }

            //推送通讯报警
            var signAlarm = message.filter((item, index) => {
                if (item.AlarmEnum === 1) {
                    return item;
                }
            });
            if (signAlarm.length > 0) {
                for (var j = 0; j < signAlarm.length; j++) {

                    //var bgColor = "";
                    var signAlarmLevel = signAlarm[j].AlarmLevel; //报警级别
                    switch (signAlarmLevel) {
                        case 0:
                            //bgColor = "alert-secondary";
                            //signAlarmLevel = "无";
                            signAlarmLevel = "<span class=\"badge badge-light\">无</span>";
                            break;
                        case 1:
                            //bgColor = "alert-info";
                            //signAlarmLevel = "普通";
                            signAlarmLevel = "<span class=\"badge badge-info\">普通</span>";
                            break;
                        case 2:
                            //bgColor = "alert-warning";
                            //signAlarmLevel = "重要";
                            signAlarmLevel = "<span class=\"badge badge-warning\">重要</span>";
                            break;
                        case 3:
                            //bgColor = "alert-danger";
                            //signAlarmLevel = "错误";
                            signAlarmLevel = "<span class=\"badge badge-danger\">错误</span>";
                            break;
                        default:
                            signAlarmLevel = "";
                            break;
                    }

                    var signHtml = "";
                    signHtml += "<tr class=\"bg-simple-danger\">";
                    signHtml += "<td class=\"text-high-danger\">" + signAlarm[j].Msg + "</td>";
                    signHtml += "<td>" + signAlarmLevel + "</td>";
                    signHtml += "<td class=\"text-high-danger\">" + signAlarm[j].Time + "</td>";
                    signHtml += "</tr>";

                    var signTbody = $("#notice-sidebar #signAlarm table tbody");
                    var signRows = signTbody.find("tr").length;
                    if (signRows <= 0) {
                        signTbody.append(signHtml); //表格结尾插入内容
                    } else {
                        signTbody.find("tr:first").before(signHtml); //第一行前面插入内容
                    }

                    //检查表格行数
                    if (signRows >= 50) {
                        signTbody.find("tr:last").remove(); //移除最后一行
                    }
                }
            }
        }
    };

    //心跳包
    kibaHub.client.pushtime = function (message) {
        console.log(message)
    }


    function drawnew() {
        //  var infos = newdata();
        var html = '';

        html += ' <div class="str1 str_wrap " style="height:20px; width:205px;display: flex; " ><a class="nav-link fa fa-tasks" style="color:white" onclick="javascript:void(0);"  href="javascript:void(0);"></a><iframe src="new.html" scrolling="no" allowtransparency=true frameborder="no" style="height:20px;width:500px;"> </iframe></div>';
       

        return html;
    }


    function openNews()
    {
        $.layer.open({ title: "新闻", content: "news.html", width: "600px", height: "600px" });
    }
    function SharkFC(infos)
    {
        if (infos.length < 1) {
            return;
        }
        for (var i in infos) {
            var info = infos[i];
            if (info.AlarmLevel == 2 || info.AlarmLevel == 3) {
                if (info.IsRead !== true) { 
                    if (document.getElementById("notice-sidebar").style.width <= 400) {
                        isShark = true;
                        if (isShark) {
                            $(".notice").css("animation-play-state", 'running');
                        }
                        isShark = false
                        break;
                    } else {

                        $('.notice').css("animation-play-state", 'paused');
                        isShark = false
                    }
                }
            }
        }
    }


    $(document).ready(function () {

        $(".metro-operation-status").mouseenter(function () {
            var selectname = $(this).attr("selectname");
            console.log(selectname);
            //$(this).find(".title-container").show();
        }).mouseleave(function () {
            // $(this).find(".title-container").hide();
        });
    });

})(jQuery);










