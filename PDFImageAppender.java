package com.cmf.snapshot.util;

import java.io.File;
import java.io.IOException;

import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.PDPageContentStream;
import org.apache.pdfbox.pdmodel.graphics.image.PDImageXObject;
public class PDFImageAppender {
    public static void main(String[] args) {
        try {
            // 加载已有的PDF文档
            PDDocument document = PDDocument.load(new File("D:\\dev\\WebStorm 2023.1.1\\help\\ReferenceCard.pdf"));
            PDPage page1 = document.getPage(0);
            // 创建一个新的PDF页
            PDPage page = new PDPage(page1.getMediaBox());
            document.addPage(page);
            // 加载图片文件
            File imageFile = new File("D:\\software\\QQLive\\vnapp\\42\\42\\index\\image\\windowbg.jpg");
            // 创建一个PDImageXObject对象
            PDImageXObject image = PDImageXObject.createFromFileByExtension(imageFile, document);
            // 获取新页的内容流
            PDPageContentStream contentStream = new PDPageContentStream(document, page);
            // 获取图片的宽度和高度
            float imageWidth = image.getWidth();
            float imageHeight = image.getHeight();
            // 计算图片在新页中的位置，使其居中显示
            float x = (page1.getMediaBox().getWidth()  - imageWidth) / 2; // 图片的横坐标
            float y = (page1.getMediaBox().getHeight() - imageHeight) / 2; // 图片的纵坐标

            float width = 500; // 图片的宽度
            float height = 200; // 图片的高度
            // 在新页的内容流中绘制图片
            contentStream.drawImage(image, x, y, width, height);
            // 关闭内容流
            contentStream.close();
            // 保存新的PDF文档
            document.save("new.pdf");
            // 关闭PDF文档
            document.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        /**
         * <dependency>
         *             <groupId>org.apache.pdfbox</groupId>
         *             <artifactId>pdfbox</artifactId>
         *             <version>2.0.23</version>
         *         </dependency>
         */
    }
}